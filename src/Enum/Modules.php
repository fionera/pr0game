<?php

namespace App\Enum;

enum Modules: int
{
    case ALLIANCE = 0;
    case MISSION_ATTACK = 1;
    case BUILDING = 2;
    case RESEARCH = 3;
    case SHIPYARD_FLEET = 4;
    case SHIPYARD_DEFENSIVE = 5;
    case BUDDYLIST = 6;
    case FLEET_TABLE = 9;
    case FLEET_EVENTS = 10;
    case GALAXY = 11;
    case BATTLEHALL = 12;
    case INFORMATION = 14;
    case IMPERIUM = 15;
    case MESSAGES = 16;
    case NOTICE = 17;
    case PHALANX = 19;
    case PLAYERCARD = 20;
    case BANLIST = 21;
    case RECORDS = 22;
    case RESSOURCE_LIST = 23;
    case MISSION_SPY = 24;
    case STATISTICS = 25;
    case SEARCH = 26;
    case SUPPORT = 27;
    case TECHTREE = 28;
    case MISSION_DESTROY = 29;
    case MISSION_EXPEDITION = 30;
    case MISSION_RECYCLE = 32;
    case MISSION_HOLD = 33;
    case MISSION_TRANSPORT = 34;
    case MISSION_COLONY = 35;
    case MISSION_STATION = 36;
    case BANNER = 37;
    case FLEET_TRADER = 38;
    case SIMULATOR = 39;
    case MISSILEATTACK = 40;
    case SHORTCUTS = 41;
    case MISSION_ACS = 42;
    case MISSION_TRADE = 43;
    case MISSION_TRANSFER = 44;
    case MARKET_TRADE = 45;
    case MARKET_TRANSFER = 46;
    case AMOUNT = 47;
}
