<?php

namespace App\Enum;

enum Mission: int
{
    case ATTACK = 1;
    case ACS = 2;
    case TRANSPORT = 3;
    case STATION = 4;
    case HOLD = 5;
    case SPY = 6;
    case COLONISATION = 7;
    case RECYCLING = 8;
    case DESTRUCTION = 9;
    case MISSILE = 10;
    case EXPEDITION = 15;
    case TRADE = 16;
    case TRANSFER = 17;
}