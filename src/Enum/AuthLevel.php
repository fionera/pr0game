<?php

namespace App\Enum;

enum AuthLevel: int
{

    case Admin = 3;
    case Operator = 2;
    case Moderator = 1;
    case User = 0;
}
