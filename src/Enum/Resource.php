<?php

namespace App\Enum;

enum Resource: int
{
    //FROM DB
case METAL_MINE = 1;
case CRYSTAL_MINE = 2;
case DEUTERIUM_SINTETIZER = 3;
case SOLAR_PLANT = 4;
case UNIVERSITY = 6;
case FUSION_PLANT = 12;
case ROBOT_FACTORY = 14;
case NANO_FACTORY = 15;
case HANGAR = 21;
case METAL_STORE = 22;
case CRYSTAL_STORE = 23;
case DEUTERIUM_STORE = 24;
case LABORATORY = 31;
case TERRAFORMER = 33;
case ALLY_DEPOSIT = 34;
case MONDBASIS = 41;
case PHALANX = 42;
case SPRUNGTOR = 43;
case SILO = 44;
case SPY_TECH = 106;
case COMPUTER_TECH = 108;
case MILITARY_TECH = 109;
case SHIELD_TECH = 110;
case DEFENCE_TECH = 111;
case ENERGY_TECH = 113;
case HYPERSPACE_TECH = 114;
case COMBUSTION_TECH = 115;
case IMPULSE_MOTOR_TECH = 117;
case HYPERSPACE_MOTOR_TECH = 118;
case LASER_TECH = 120;
case IONIC_TECH = 121;
case BUSTER_TECH = 122;
case INTERGALACTIC_TECH = 123;
case EXPEDITION_TECH = 124;
case METAL_PROC_TECH = 131;
case CRYSTAL_PROC_TECH = 132;
case DEUTERIUM_PROC_TECH = 133;
case GRAVITON_TECH = 199;
case SMALL_SHIP_CARGO = 202;
case BIG_SHIP_CARGO = 203;
case LIGHT_HUNTER = 204;
case HEAVY_HUNTER = 205;
case CRUSHER = 206;
case BATTLE_SHIP = 207;
case COLONIZER = 208;
case RECYCLER = 209;
case SPY_SONDE = 210;
case BOMBER_SHIP = 211;
case SOLAR_SATELIT = 212;
case DESTRUCTOR = 213;
case DEARTH_STAR = 214;
case BATTLESHIP = 215;
case LUNE_NOIR = 216;
case EV_TRANSPORTER = 217;
case STAR_CRASHER = 218;
case GIGA_RECYKLER = 219;
case MISIL_LAUNCHER = 401;
case SMALL_LASER = 402;
case BIG_LASER = 403;
case GAUSS_CANYON = 404;
case IONIC_CANYON = 405;
case BUSTER_CANYON = 406;
case SMALL_PROTECTION_SHIELD = 407;
case BIG_PROTECTION_SHIELD = 408;
case PLANET_PROTECTOR = 409;
case GRAVITON_CANYON = 410;
case ORBITAL_STATION = 411;
case INTERCEPTOR_MISIL = 502;
case INTERPLANETARY_MISIL = 503;

    //FROM VARS FILE
    case METAL = 901;
    case CRYSTAL = 902;
    case DEUT = 903;
}