<?php

namespace App\Enum;

enum Element: int
{
    case BUILD = 1; # ID 0 - 99
    case TECH = 2; # ID 101 - 199
    case FLEET = 4; # ID 201 - 399
    case DEFENSIVE = 8; # ID 401 - 599
    case OFFICIER = 16; # ID 601 - 699
    case BONUS = 32; # ID 701 - 799
    case RACE = 64; # ID 801 - 899
    case PLANET_RESOURCE = 128; # ID 901 - 949
    case USER_RESOURCE = 256; # ID 951 - 999

// .. 512, 1024, 2048, 4096, 8192, 16384, 32768
    case PRODUCTION = 65536;
    case STORAGE = 131072;
    case ONEPERPLANET = 262144;
    case BOUNS = 524288;
    case BUILD_ON_PLANET = 1048576;
    case BUILD_ON_MOONS = 2097152;
    case RESOURCE_ON_TF = 4194304;
    case RESOURCE_ON_FLEET = 8388608;
    case RESOURCE_ON_STEAL = 16777216;
}