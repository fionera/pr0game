<?php

namespace App\Enum;

enum Fleet: int
{
    case OUTWARD = 0;
    case RETURN = 1;
    case HOLD = 2;
}