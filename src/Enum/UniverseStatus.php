<?php

namespace App\Enum;

enum UniverseStatus: int
{
    case STATUS_OPEN = 0;
    case STATUS_CLOSED = 1;
    case STATUS_REG_ONLY = 2;
    case STATUS_LOGIN_ONLY = 3;
}