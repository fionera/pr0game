<?php

namespace App\Enum;

enum Ship: int
{
    case SMALL_CARGO = 202;
    case LARGE_CARGO = 203;
    case LIGHT_FIGHTER = 204;
    case HEAVY_FIGHTER = 205;
    case CRUISER = 206;
    case BATTLESHIP = 207;
    case COLOSHIP = 208;
    case RECYCLER = 209;
    case PROBE = 210;
    case BOMBER = 211;
    case SOLSAT = 212;
    case DESTROYER = 213;
    case RIP = 214;
    case BATTLECRUISER = 215;
}