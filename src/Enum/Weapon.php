<?php

namespace App\Enum;

enum Weapon: int
{
    case MISSILE_LAUNCHER = 401;
    case LIGHT_LASER_TURRET = 402;
    case HEAVY_LASER_TURRET = 403;
    case GAUSS_CANNON = 404;
    case ION_CANNON = 405;
    case PLASMA_CANNON = 406;
    case SMALL_SHIELD_DOME = 407;
    case LARGE_SHIELD_DOME = 408;
}