<?php

namespace App\Controller\Game;

use App\Entity\Fleets;
use App\Entity\Planet;
use App\Entity\Statpoints;
use App\Entity\UniverseConfig;
use App\Entity\User;
use App\Enum\AuthLevel;
use App\Enum\Resource;
use App\Repository\PlanetRepository;
use App\Service\BuildInfoService;
use App\Service\ColorService;
use App\Service\TextFormatting;
use App\Structs\PlanetStruct;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class OverviewController extends AbstractController
{
    #[Route('/game/', name: 'app_game_overview')]
    public function overview(
        EntityManagerInterface $entityManager,
        User                   $user,
        Request                $request,
        TranslatorInterface    $translator,
        BuildInfoService       $buildInfoService,
        ColorService           $colorService,
        ParameterBagInterface  $params,
    ): Response
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        /** @var PlanetRepository $planetRepository */
        $planetRepository = $entityManager->getRepository(Planet::class);
        /** @var Planet[] $planets */
        $planets = $planetRepository->createQueryBuilder('p')
            ->where('p.destroyed = :destroyed')
            ->andWhere('p.idOwner = :owner')
            ->setParameter('destroyed', 0)
            ->setParameter('owner', $user->getId())
            ->getQuery()->getResult();

        //TODO: Correct planet
        $currentPlanet = $user->getMainPlanet();
        $allPlanets = [];
        foreach ($planets as $planet) {
            if ($planet->getId() === $currentPlanet->getId()) {
                continue;
            }

            $allPlanets[] = [
                'id' => $planet->getId(),
                'name' => $planet->getName(),
                'image' => $planet->getImage(),
                'build' => $buildInfoService->getBuildingsInfo($user, $planet),
                'type' => $planet->getPlanetType(),
                'galaxy' => $planet->isGalaxy(),
                'system' => $planet->getSystem(),
                'planet' => $planet->isPlanet(),
            ];
        }

        /** @var Statpoints|null $statData */
        $statData = $entityManager->getRepository(Statpoints::class)
            // TODO: Remove magic number
            ->findOneBy(['owner' => $user->getId(), 'type' => 1]);

        $rankInfo = "-";
        if ($statData !== null) {
            $rankInfo = sprintf(
                $translator->trans('ov_userrank_info'),
                TextFormatting::prettyNumber($statData->getTotalPoints()),
                $translator->trans('ov_place'),
                $statData->getTotalRank(),
                $statData->getTotalRank(),
                $translator->trans('ov_of'),
                $universe->getUsersAmount()
            );
        }

        /** @var EntityRepository $userRepository */
        $userRepository = $entityManager->getRepository(User::class);
        $usersOnline = $userRepository->createQueryBuilder('u')
            ->select('count(u)')
            ->where('u.onlinetime >= :timestamp')
            ->setParameter('timestamp', time() - 60 * 15)
            ->getQuery()->getSingleScalarResult();

        /** @var EntityRepository $fleetsRepository */
        $fleetsRepository = $entityManager->getRepository(Fleets::class);
        $fleetsCount = $fleetsRepository->createQueryBuilder('f')
            ->select('count(f)')
            ->getQuery()->getSingleScalarResult();

        return $this->render('game/page.overview.default.tpl', [
            'bodyclass' => 'full',
            'umode' => $user->isUrlaubsModus(),
            'rankInfo' => $rankInfo,
            'is_news' => $universe->isOverviewnewsframe(),
            'news' => TextFormatting::makeBR($universe->getOverviewnewstext()),
            'usersOnline' => $usersOnline,
            'fleetsOnline' => $fleetsCount,
            'planetname' => $currentPlanet->getName(),
            'planetimage' => $currentPlanet->getImage(),
            'galaxy' => $currentPlanet->isGalaxy(),
            'system' => $currentPlanet->getSystem(),
            'planet' => $currentPlanet->isPlanet(),
            'planet_type' => $currentPlanet->getPlanetType(),
            'username' => $user->getUsername(),
            'userid' => $user->getId(),
            'buildInfo' => $buildInfoService->getPlanetInfo($user, $currentPlanet),
            'Moon' => $buildInfoService->getMoonInfo($user, $currentPlanet),
            // TODO: 'fleets' => $this->getFleets(),
            'fleets' => [],
            'AllPlanets' => $allPlanets,
            'AdminsOnline' => [], //TODO
            'messages' => false, //TODO
            'planet_diameter' => TextFormatting::prettyNumber($currentPlanet->getDiameter()),
            'planet_field_current' => $currentPlanet->getFieldCurrent(),
            'planet_field_max' => PlanetStruct::getMaxPlanetFields($currentPlanet, $params),
            'planet_temp_min' => $currentPlanet->getTempMin(),
            'planet_temp_max' => $currentPlanet->getTempMax(),
            'ref_active' => $universe->getRefActive(),
            'ref_minpoints' => $universe->getRefMinpoints(),
            'RefLinks' => [], //TODO
            'servertime' => (new \DateTime())
                ->setTimezone(new \DateTimeZone($user->getTimezone()))
                ->format('d. M Y, H:i:s'),
            //TODO: 'path' => HTTP_PATH,
            'colors' => $colorService->getColors($user),
            'signalColors'  => $colorService->player_signal_colors($user),
            'image' => $currentPlanet->getImage(),
            'PlanetSelect' => $allPlanets,
            'current_pid' => $currentPlanet->getId(),
            'resourceTable' => [], //TODO
            'avatar' => '/styles/resource/images/user.png', //TODO: Profile Pic
            'shortlyNumber' => 0,
            'vmode' => $user->isUrlaubsModus(),
            'hasGate' => $currentPlanet->getSprungtor() > 0,
            'new_message' => '',
            'new_allyrequests' => '',
            'authlevel' => AuthLevel::User->value,
            'hasAdminAccess' => false,
            'uni_status' => $universe->getUniverseStatus()->value,
            'delete' => false,
            'vacation' => false,
            'cronjobs' => false,
            'ga_active' => false,
            'debug' => false,
        ]);
    }
}