<?php

namespace App\Controller;

use App\Entity\Banned;
use App\Entity\News;
use App\Entity\UniverseConfig;
use App\Service\BattlehallService;
use App\Service\TextFormatting;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Parsedown;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class FrontpageController extends AbstractController
{
    #[Route('/login', name: 'app_login')]
    public function login(): Response
    {
        return $this->forward('App\Controller\FrontpageController::index');
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(Security $security): Response
    {
        if ($security->isGranted('IS_AUTHENTICATED_FULLY')) {
            $security->logout(false);
        }
        return $this->redirectToRoute('app_frontpage_index');
    }


    #[Route('/', name: 'app_frontpage_index')]
    public function index(
        Request             $request,
        TranslatorInterface $translator,
        AuthenticationUtils $authenticationUtils,
    ): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_game_index');
        }

        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        $referralID = $request->get('ref', 0);
        if (!empty($referralID)) {
            //TODO: Refs
            //$this->redirectTo('index.php?page=register&referralID=' . $referralID);
            die();
        }

        $errorCode = false;
        if ($err = $authenticationUtils->getLastAuthenticationError()) {
            $code = match ($err::class) {
                BadCredentialsException::class => 1,
                default => null,
            };
            // We dont have an error message for all errors
            if ($code !== null) {
                $errorCode = $translator->trans('login_error_' . $code);
            } else {
                dd($err);
            }
        }

        return $this->render('login/page.index.default.tpl', [
            'bodyclass' => 'light',
            'code' => $errorCode,
            'descHeader' => sprintf($translator->trans('loginWelcome'), $universe->getGameName()),
            'descText' => sprintf($translator->trans('loginServerDesc'), $universe->getGameName()),
            'gameInformations' => explode("\n", $translator->trans('gameInformations')),
            'loginInfo' => sprintf($translator->trans('loginInfo'), '<a href="' . $this->generateUrl('app_frontpage_rules') . '">' . $translator->trans('menu_rules') . '</a>'),
            'countcaptchakey' => $universe->getRecaptchapubkey() === null ? 0 : 1,
        ]);
    }

    #[Route('/rules', name: 'app_frontpage_rules')]
    public function rules(
        Request             $request,
        TranslatorInterface $translator,
    ): Response
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        return $this->render('login/page.rules.default.tpl', [
            'rules' => sprintf($translator->trans('rules'), $universe->getGitIssuesLink()),
        ]);
    }

    #[Route('/news', name: 'app_frontpage_news')]
    public function news(
        TranslatorInterface    $translator,
        EntityManagerInterface $entityManager
    ): Response
    {
        /** @var News[] $allNews */
        $allNews = $entityManager->getRepository(News::class)->findBy([], ['id' => 'DESC']);

        return $this->render('login/page.news.default.tpl', [
            'newsList' => array_map(static fn(News $news) => [
                'title' => $news->getTitle(),
                'from' => sprintf(
                    $translator->trans('news_from'),
                    _date($translator->trans('php_tdformat'), $news->getDate()),
                    $news->getUser()
                ),
                'text' => TextFormatting::makeBR($news->getText()),
            ], $allNews),
        ]);
    }

    #[Route('/disclaimer', name: 'app_frontpage_disclaimer')]
    public function disclaimer(
        Request $request,
    ): Response
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        return $this->render('login/page.disclamer.default.tpl', [
            'disclamerAddress' => TextFormatting::makeBR($universe->getDisclameraddress()),
            'disclamerPhone' => $universe->getDisclamerphone(),
            'disclamerMail' => $universe->getDisclamermail(),
            'disclamerNotice' => $universe->getDisclamernotice(),
        ]);
    }

    #[Route('/community', name: 'app_frontpage_community')]
    public function community(
        Request             $request,
        TranslatorInterface $translator
    ): RedirectResponse
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        $communityURL = $universe->getForumUrl();
        if (!filter_var($communityURL, FILTER_VALIDATE_URL)) {
            throw new \RuntimeException($translator->trans('bad_forum_url'));
        }

        return $this->redirect($communityURL);
    }

    #[Route('/bans', name: 'app_frontpage_ban')]
    public function bans(
        Request                $request,
        EntityManagerInterface $entityManager,
        TranslatorInterface    $translator,
    )
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);
        $page = $request->get('page', 1);

        /** @var EntityRepository $banned */
        $banned = $entityManager->getRepository(Banned::class);
        $query = $banned->createQueryBuilder('b')
            ->orderBy('b.time', 'DESC')
            ->where('b.universe = :universe')
            ->setParameter('universe', $universe->getID())
            ->getQuery();

        $paginator = new Paginator($query);

        $pageSize = 25;
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1))
            ->setMaxResults($pageSize);

        $banList = [];
        /** @var Banned $ban */
        foreach ($paginator as $ban) {
            $banList[] = [
                'player' => $ban->getWho(),
                'theme' => $ban->getTheme(),
                'from' => (new \DateTime())
                    ->setTimezone(new \DateTimeZone($universe->getTimezone()))
                    ->setTimestamp($ban->getTime())
                    ->format('d. M Y, H:i:s'),
                'to' => (new \DateTime())
                    ->setTimezone(new \DateTimeZone($universe->getTimezone()))
                    ->setTimestamp($ban->getLonger())
                    ->format('d. M Y, H:i:s'),
                'admin' => $ban->getAuthor(),
                'mail' => $ban->getEmail(),
                'info' => sprintf($translator->trans('bn_writemail'), $ban->getAuthor()),
            ];
        }

        return $this->render('login/page.banList.default.tpl', [
            'banList' => $banList,
            'banCount' => $totalItems,
            'page' => $page,
            'maxPage' => $pagesCount,
        ]);
    }

    #[Route('/battlehall', name: 'app_frontpage_battlehall')]
    public function battleHall(
        Request           $request,
        BattlehallService $battlehallService
    ): Response
    {
        /** @var UniverseConfig $universe */
        $universe = $request->get(UniverseConfig::REQUEST_ATTRIBUTE);

        return $this->render('login/page.battleHall.default.tpl', [
            'hallList' => $battlehallService->getBattleHallList($universe),
        ]);
    }

    #[Route('/changelog', name: 'app_frontpage_changelog')]
    public function changelog(
        ParameterBagInterface $params
    ): Response
    {
        return $this->render('game/page.changelog.default.tpl', [
            'ChangelogList' => (new Parsedown())->text(
                file_get_contents($params->get('kernel.project_dir') . '/CHANGES.md')
            )
        ]);
    }
}
