<?php

namespace App\Entity;

use App\Enum\AuthLevel;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 */
#[ORM\Table(name: 'users')]
#[ORM\Index(name: 'ally_id', columns: ['ally_id'])]
#[ORM\Index(name: 'authlevel', columns: ['authlevel'])]
#[ORM\Index(name: 'ref_bonus', columns: ['ref_bonus'])]
#[ORM\Index(name: 'universe', columns: ['universe', 'username', 'password', 'onlinetime', 'authlevel'])]
#[ORM\Entity]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    #[ORM\Column(name: 'username', type: 'string', length: 32, nullable: false)]
    private ?string $username = null;

    #[ORM\Column(name: 'password', type: 'string', length: 60, nullable: false)]
    private ?string $password = null;

    #[ORM\Column(name: 'email', type: 'string', length: 64, nullable: false)]
    private string $email = '';

    #[ORM\Column(name: 'lang', type: 'string', length: 2, nullable: false, options: ['default' => 'de'])]
    private string $lang = 'de';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'authattack', type: 'boolean', nullable: false)]
    private $authattack = '0';

    /**
     * @var AuthLevel
     */
    #[ORM\Column(name: 'authlevel', type: 'smallint', nullable: false, enumType: AuthLevel::class)]
    private AuthLevel $authLevel = AuthLevel::User;

    #[ORM\Column(name: 'rights', type: 'text', length: 65535, nullable: true)]
    private ?string $rights = null;

    #[ORM\OneToOne(targetEntity: Planet::class)]
    #[ORM\JoinColumn(name: 'id_planet', referencedColumnName: 'id', nullable: false,options: ['unsigned' => true])]
    private Planet $mainPlanet;

    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private ?bool $universe = null;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'galaxy', type: 'boolean', nullable: false)]
    private $galaxy = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'system', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private $system = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'planet', type: 'boolean', nullable: false)]
    private $planet = '0';

    #[ORM\Column(name: 'user_lastip', type: 'string', length: 40, nullable: false)]
    private string $userLastip = '';

    #[ORM\Column(name: 'ip_at_reg', type: 'string', length: 40, nullable: false)]
    private string $ipAtReg = '';

    /**
     * @var int
     */
    #[ORM\Column(name: 'register_time', type: 'integer', nullable: false)]
    private $registerTime = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'onlinetime', type: 'integer', nullable: false)]
    private $onlinetime = '0';

    #[ORM\Column(name: 'dpath', type: 'string', length: 20, nullable: false, options: ['default' => 'nova'])]
    private string $dpath = 'nova';

    #[ORM\Column(name: 'timezone', type: 'string', length: 32, nullable: false, options: ['default' => 'Europe/London'])]
    private string $timezone = 'Europe/London';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'planet_sort', type: 'boolean', nullable: false)]
    private $planetSort = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'planet_sort_order', type: 'boolean', nullable: false)]
    private $planetSortOrder = '0';

    #[ORM\Column(name: 'spio_anz', type: 'integer', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private int $spioAnz = 1;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'settings_fleetactions', type: 'boolean', nullable: false, options: ['default' => 3])]
    private $settingsFleetactions = '3';

    #[ORM\Column(name: 'settings_esp', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $settingsEsp = true;

    #[ORM\Column(name: 'settings_wri', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $settingsWri = true;

    #[ORM\Column(name: 'settings_bud', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $settingsBud = true;

    #[ORM\Column(name: 'settings_mis', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $settingsMis = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'settings_blockPM', type: 'boolean', nullable: false)]
    private $settingsBlockpm = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'urlaubs_modus', type: 'boolean', nullable: false)]
    private $urlaubsModus = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'urlaubs_until', type: 'integer', nullable: false)]
    private $urlaubsUntil = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'urlaubs_start', type: 'integer', nullable: false)]
    private $urlaubsStart = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'db_deaktjava', type: 'integer', nullable: false)]
    private $dbDeaktjava = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'b_tech_planet', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $bTechPlanet = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'b_tech', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $bTech = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'b_tech_id', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private $bTechId = '0';

    #[ORM\Column(name: 'b_tech_queue', type: 'text', length: 65535, nullable: true)]
    private ?string $bTechQueue = null;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'spy_tech', type: 'boolean', nullable: false)]
    private $spyTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'computer_tech', type: 'boolean', nullable: false)]
    private $computerTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'military_tech', type: 'boolean', nullable: false)]
    private $militaryTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'defence_tech', type: 'boolean', nullable: false)]
    private $defenceTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'shield_tech', type: 'boolean', nullable: false)]
    private $shieldTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'energy_tech', type: 'boolean', nullable: false)]
    private $energyTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'hyperspace_tech', type: 'boolean', nullable: false)]
    private $hyperspaceTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'combustion_tech', type: 'boolean', nullable: false)]
    private $combustionTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'impulse_motor_tech', type: 'boolean', nullable: false)]
    private $impulseMotorTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'hyperspace_motor_tech', type: 'boolean', nullable: false)]
    private $hyperspaceMotorTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'laser_tech', type: 'boolean', nullable: false)]
    private $laserTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'ionic_tech', type: 'boolean', nullable: false)]
    private $ionicTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'buster_tech', type: 'boolean', nullable: false)]
    private $busterTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'intergalactic_tech', type: 'boolean', nullable: false)]
    private $intergalacticTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'expedition_tech', type: 'boolean', nullable: false)]
    private $expeditionTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'metal_proc_tech', type: 'boolean', nullable: false)]
    private $metalProcTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'crystal_proc_tech', type: 'boolean', nullable: false)]
    private $crystalProcTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'deuterium_proc_tech', type: 'boolean', nullable: false)]
    private $deuteriumProcTech = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'graviton_tech', type: 'boolean', nullable: false)]
    private $gravitonTech = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'ally_id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $allyId = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'ally_register_time', type: 'integer', nullable: false)]
    private $allyRegisterTime = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'ally_rank_id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $allyRankId = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'bana', type: 'boolean', nullable: false)]
    private $bana = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'banaday', type: 'integer', nullable: false)]
    private $banaday = '0';

    #[ORM\Column(name: 'hof', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $hof = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'spyMessagesMode', type: 'boolean', nullable: false)]
    private $spymessagesmode = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'wons', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $wons = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'loos', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $loos = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'draws', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $draws = '0';

    /**
     * @var float
     */
    #[ORM\Column(name: 'kbmetal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private $kbmetal = '0';

    /**
     * @var float
     */
    #[ORM\Column(name: 'kbcrystal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private $kbcrystal = '0';

    /**
     * @var float
     */
    #[ORM\Column(name: 'lostunits', type: 'float', precision: 50, scale: 0, nullable: false)]
    private $lostunits = '0';

    /**
     * @var float
     */
    #[ORM\Column(name: 'desunits', type: 'float', precision: 50, scale: 0, nullable: false)]
    private $desunits = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'uctime', type: 'integer', nullable: false)]
    private $uctime = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'setmail', type: 'integer', nullable: false)]
    private $setmail = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'ref_id', type: 'integer', nullable: false)]
    private $refId = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'ref_bonus', type: 'boolean', nullable: false)]
    private $refBonus = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'inactive_mail', type: 'boolean', nullable: false)]
    private $inactiveMail = '0';

    #[ORM\Column(name: 'colorMission2friend', type: 'string', length: 7, nullable: false, options: ['default' => '#ff00ff'])]
    private string $colormission2friend = '#ff00ff';

    #[ORM\Column(name: 'colorMission1Own', type: 'string', length: 7, nullable: false, options: ['default' => '#66cc33'])]
    private string $colormission1own = '#66cc33';

    #[ORM\Column(name: 'colorMission2Own', type: 'string', length: 7, nullable: false, options: ['default' => '#339966'])]
    private string $colormission2own = '#339966';

    #[ORM\Column(name: 'colorMission3Own', type: 'string', length: 7, nullable: false, options: ['default' => '#5bf1c2'])]
    private string $colormission3own = '#5bf1c2';

    #[ORM\Column(name: 'colorMission4Own', type: 'string', length: 7, nullable: false, options: ['default' => '#cf79de'])]
    private string $colormission4own = '#cf79de';

    #[ORM\Column(name: 'colorMission5Own', type: 'string', length: 7, nullable: false, options: ['default' => '#80a0c0'])]
    private string $colormission5own = '#80a0c0';

    #[ORM\Column(name: 'colorMission6Own', type: 'string', length: 7, nullable: false, options: ['default' => '#ffcc66'])]
    private string $colormission6own = '#ffcc66';

    #[ORM\Column(name: 'colorMission7Own', type: 'string', length: 7, nullable: false, options: ['default' => '#c1c1c1'])]
    private string $colormission7own = '#c1c1c1';

    #[ORM\Column(name: 'colorMission7OwnReturn', type: 'string', length: 7, nullable: false, options: ['default' => '#cf79de'])]
    private string $colormission7ownreturn = '#cf79de';

    #[ORM\Column(name: 'colorMission8Own', type: 'string', length: 7, nullable: false, options: ['default' => '#ceff68'])]
    private string $colormission8own = '#ceff68';

    #[ORM\Column(name: 'colorMission9Own', type: 'string', length: 7, nullable: false, options: ['default' => '#ffff99'])]
    private string $colormission9own = '#ffff99';

    #[ORM\Column(name: 'colorMission10Own', type: 'string', length: 7, nullable: false, options: ['default' => '#ffcc66'])]
    private string $colormission10own = '#ffcc66';

    #[ORM\Column(name: 'colorMission15Own', type: 'string', length: 7, nullable: false, options: ['default' => '#5bf1c2'])]
    private string $colormission15own = '#5bf1c2';

    #[ORM\Column(name: 'colorMission16Own', type: 'string', length: 7, nullable: false, options: ['default' => '#5bf1c2'])]
    private string $colormission16own = '#5bf1c2';

    #[ORM\Column(name: 'colorMission17Own', type: 'string', length: 7, nullable: false, options: ['default' => '#5bf1c2'])]
    private string $colormission17own = '#5bf1c2';

    #[ORM\Column(name: 'colorMissionReturnOwn', type: 'string', length: 7, nullable: false, options: ['default' => '#6e8eea'])]
    private string $colormissionreturnown = '#6e8eea';

    #[ORM\Column(name: 'colorMission1Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#ff0000'])]
    private string $colormission1foreign = '#ff0000';

    #[ORM\Column(name: 'colorMission2Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#aa0000'])]
    private string $colormission2foreign = '#aa0000';

    #[ORM\Column(name: 'colorMission3Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#00ff00'])]
    private string $colormission3foreign = '#00ff00';

    #[ORM\Column(name: 'colorMission4Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#ad57bc'])]
    private string $colormission4foreign = '#ad57bc';

    #[ORM\Column(name: 'colorMission5Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#3399cc'])]
    private string $colormission5foreign = '#3399cc';

    #[ORM\Column(name: 'colorMission6Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#ff6600'])]
    private string $colormission6foreign = '#ff6600';

    #[ORM\Column(name: 'colorMission7Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#00ff00'])]
    private string $colormission7foreign = '#00ff00';

    #[ORM\Column(name: 'colorMission8Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#acdd46'])]
    private string $colormission8foreign = '#acdd46';

    #[ORM\Column(name: 'colorMission9Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#dddd77'])]
    private string $colormission9foreign = '#dddd77';

    #[ORM\Column(name: 'colorMission10Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#ff6600'])]
    private string $colormission10foreign = '#ff6600';

    #[ORM\Column(name: 'colorMission15Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#39d0a0'])]
    private string $colormission15foreign = '#39d0a0';

    #[ORM\Column(name: 'colorMission16Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#39d0a0'])]
    private string $colormission16foreign = '#39d0a0';

    #[ORM\Column(name: 'colorMission17Foreign', type: 'string', length: 7, nullable: false, options: ['default' => '#39d0a0'])]
    private string $colormission17foreign = '#39d0a0';

    #[ORM\Column(name: 'colorMissionReturnForeign', type: 'string', length: 7, nullable: false, options: ['default' => '#6e8eea'])]
    private string $colormissionreturnforeign = '#6e8eea';

    #[ORM\Column(name: 'colorStaticTimer', type: 'string', length: 7, nullable: false, options: ['default' => '#ffff00'])]
    private string $colorstatictimer = '#ffff00';

    #[ORM\Column(name: 'colorPositive', type: 'string', length: 7, nullable: false, options: ['default' => '#00ff00'])]
    private string $colorpositive = '#00ff00';

    #[ORM\Column(name: 'colorNegative', type: 'string', length: 7, nullable: false, options: ['default' => '#ff0000'])]
    private string $colornegative = '#ff0000';

    #[ORM\Column(name: 'colorNeutral', type: 'string', length: 7, nullable: false, options: ['default' => '#ffd600'])]
    private string $colorneutral = '#ffd600';

    #[ORM\Column(name: 'prioMission1', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $priomission1 = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission2', type: 'boolean', nullable: false, options: ['default' => 2])]
    private $priomission2 = '2';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission3', type: 'boolean', nullable: false)]
    private $priomission3 = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission4', type: 'boolean', nullable: false, options: ['default' => 3])]
    private $priomission4 = '3';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission5', type: 'boolean', nullable: false, options: ['default' => 4])]
    private $priomission5 = '4';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission6', type: 'boolean', nullable: false, options: ['default' => 5])]
    private $priomission6 = '5';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission7', type: 'boolean', nullable: false, options: ['default' => 6])]
    private $priomission7 = '6';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission8', type: 'boolean', nullable: false, options: ['default' => 7])]
    private $priomission8 = '7';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission9', type: 'boolean', nullable: false, options: ['default' => 8])]
    private $priomission9 = '8';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'prioMission17', type: 'boolean', nullable: false, options: ['default' => 9])]
    private $priomission17 = '9';

    #[ORM\Column(name: 'stb_small_ress', type: 'integer', nullable: false, options: ['default' => 500])]
    private int $stbSmallRess = 500;

    #[ORM\Column(name: 'stb_med_ress', type: 'integer', nullable: false, options: ['default' => 3000])]
    private int $stbMedRess = 3000;

    #[ORM\Column(name: 'stb_big_ress', type: 'integer', nullable: false, options: ['default' => 7000])]
    private int $stbBigRess = 7000;

    #[ORM\Column(name: 'stb_small_time', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $stbSmallTime = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'stb_med_time', type: 'boolean', nullable: false, options: ['default' => 2])]
    private $stbMedTime = '2';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'stb_big_time', type: 'boolean', nullable: false, options: ['default' => 3])]
    private $stbBigTime = '3';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'stb_enabled', type: 'boolean', nullable: false)]
    private $stbEnabled = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'records_optIn', type: 'boolean', nullable: false)]
    private $recordsOptin = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function isAuthattack(): ?bool
    {
        return $this->authattack;
    }

    public function setAuthattack(bool $authattack): self
    {
        $this->authattack = $authattack;

        return $this;
    }

    public function isAuthlevel(): ?bool
    {
        return $this->authLevel;
    }

    public function setAuthLevel(bool $authLevel): self
    {
        $this->authLevel = $authLevel;

        return $this;
    }

    public function getRights(): ?string
    {
        return $this->rights;
    }

    public function setRights(?string $rights): self
    {
        $this->rights = $rights;

        return $this;
    }

    public function getMainPlanet(): Planet
    {
        return $this->mainPlanet;
    }

    public function setMainPlanet(Planet $mainPlanet): self
    {
        $this->mainPlanet = $mainPlanet;

        return $this;
    }

    public function isUniverse(): ?bool
    {
        return $this->universe;
    }

    public function setUniverse(bool $universe): self
    {
        $this->universe = $universe;

        return $this;
    }

    public function isGalaxy(): ?bool
    {
        return $this->galaxy;
    }

    public function setGalaxy(bool $galaxy): self
    {
        $this->galaxy = $galaxy;

        return $this;
    }

    public function getSystem(): ?int
    {
        return $this->system;
    }

    public function setSystem(int $system): self
    {
        $this->system = $system;

        return $this;
    }

    public function isPlanet(): ?bool
    {
        return $this->planet;
    }

    public function setPlanet(bool $planet): self
    {
        $this->planet = $planet;

        return $this;
    }

    public function getUserLastip(): ?string
    {
        return $this->userLastip;
    }

    public function setUserLastip(string $userLastip): self
    {
        $this->userLastip = $userLastip;

        return $this;
    }

    public function getIpAtReg(): ?string
    {
        return $this->ipAtReg;
    }

    public function setIpAtReg(string $ipAtReg): self
    {
        $this->ipAtReg = $ipAtReg;

        return $this;
    }

    public function getRegisterTime(): ?int
    {
        return $this->registerTime;
    }

    public function setRegisterTime(int $registerTime): self
    {
        $this->registerTime = $registerTime;

        return $this;
    }

    public function getOnlinetime(): ?int
    {
        return $this->onlinetime;
    }

    public function setOnlinetime(int $onlinetime): self
    {
        $this->onlinetime = $onlinetime;

        return $this;
    }

    public function getDpath(): ?string
    {
        return $this->dpath;
    }

    public function setDpath(string $dpath): self
    {
        $this->dpath = $dpath;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function isPlanetSort(): ?bool
    {
        return $this->planetSort;
    }

    public function setPlanetSort(bool $planetSort): self
    {
        $this->planetSort = $planetSort;

        return $this;
    }

    public function isPlanetSortOrder(): ?bool
    {
        return $this->planetSortOrder;
    }

    public function setPlanetSortOrder(bool $planetSortOrder): self
    {
        $this->planetSortOrder = $planetSortOrder;

        return $this;
    }

    public function getSpioAnz(): ?int
    {
        return $this->spioAnz;
    }

    public function setSpioAnz(int $spioAnz): self
    {
        $this->spioAnz = $spioAnz;

        return $this;
    }

    public function isSettingsFleetactions(): ?bool
    {
        return $this->settingsFleetactions;
    }

    public function setSettingsFleetactions(bool $settingsFleetactions): self
    {
        $this->settingsFleetactions = $settingsFleetactions;

        return $this;
    }

    public function isSettingsEsp(): ?bool
    {
        return $this->settingsEsp;
    }

    public function setSettingsEsp(bool $settingsEsp): self
    {
        $this->settingsEsp = $settingsEsp;

        return $this;
    }

    public function isSettingsWri(): ?bool
    {
        return $this->settingsWri;
    }

    public function setSettingsWri(bool $settingsWri): self
    {
        $this->settingsWri = $settingsWri;

        return $this;
    }

    public function isSettingsBud(): ?bool
    {
        return $this->settingsBud;
    }

    public function setSettingsBud(bool $settingsBud): self
    {
        $this->settingsBud = $settingsBud;

        return $this;
    }

    public function isSettingsMis(): ?bool
    {
        return $this->settingsMis;
    }

    public function setSettingsMis(bool $settingsMis): self
    {
        $this->settingsMis = $settingsMis;

        return $this;
    }

    public function isSettingsBlockpm(): ?bool
    {
        return $this->settingsBlockpm;
    }

    public function setSettingsBlockpm(bool $settingsBlockpm): self
    {
        $this->settingsBlockpm = $settingsBlockpm;

        return $this;
    }

    public function isUrlaubsModus(): ?bool
    {
        return $this->urlaubsModus;
    }

    public function setUrlaubsModus(bool $urlaubsModus): self
    {
        $this->urlaubsModus = $urlaubsModus;

        return $this;
    }

    public function getUrlaubsUntil(): ?int
    {
        return $this->urlaubsUntil;
    }

    public function setUrlaubsUntil(int $urlaubsUntil): self
    {
        $this->urlaubsUntil = $urlaubsUntil;

        return $this;
    }

    public function getUrlaubsStart(): ?int
    {
        return $this->urlaubsStart;
    }

    public function setUrlaubsStart(int $urlaubsStart): self
    {
        $this->urlaubsStart = $urlaubsStart;

        return $this;
    }

    public function getDbDeaktjava(): ?int
    {
        return $this->dbDeaktjava;
    }

    public function setDbDeaktjava(int $dbDeaktjava): self
    {
        $this->dbDeaktjava = $dbDeaktjava;

        return $this;
    }

    public function getBTechPlanet(): ?int
    {
        return $this->bTechPlanet;
    }

    public function setBTechPlanet(int $bTechPlanet): self
    {
        $this->bTechPlanet = $bTechPlanet;

        return $this;
    }


    public function getTechTime(): ?int
    {
        return $this->bTechId;
    }

    public function setBTechId(int $bTechId): self
    {
        $this->bTechId = $bTechId;

        return $this;
    }

    public function getTechQueue(): ?string
    {
        return $this->bTechQueue;
    }

    public function setBTechQueue(?string $bTechQueue): self
    {
        $this->bTechQueue = $bTechQueue;

        return $this;
    }

    public function isSpyTech(): ?bool
    {
        return $this->spyTech;
    }

    public function setSpyTech(bool $spyTech): self
    {
        $this->spyTech = $spyTech;

        return $this;
    }

    public function isComputerTech(): ?bool
    {
        return $this->computerTech;
    }

    public function setComputerTech(bool $computerTech): self
    {
        $this->computerTech = $computerTech;

        return $this;
    }

    public function isMilitaryTech(): ?bool
    {
        return $this->militaryTech;
    }

    public function setMilitaryTech(bool $militaryTech): self
    {
        $this->militaryTech = $militaryTech;

        return $this;
    }

    public function isDefenceTech(): ?bool
    {
        return $this->defenceTech;
    }

    public function setDefenceTech(bool $defenceTech): self
    {
        $this->defenceTech = $defenceTech;

        return $this;
    }

    public function isShieldTech(): ?bool
    {
        return $this->shieldTech;
    }

    public function setShieldTech(bool $shieldTech): self
    {
        $this->shieldTech = $shieldTech;

        return $this;
    }

    public function isEnergyTech(): ?bool
    {
        return $this->energyTech;
    }

    public function setEnergyTech(bool $energyTech): self
    {
        $this->energyTech = $energyTech;

        return $this;
    }

    public function isHyperspaceTech(): ?bool
    {
        return $this->hyperspaceTech;
    }

    public function setHyperspaceTech(bool $hyperspaceTech): self
    {
        $this->hyperspaceTech = $hyperspaceTech;

        return $this;
    }

    public function isCombustionTech(): ?bool
    {
        return $this->combustionTech;
    }

    public function setCombustionTech(bool $combustionTech): self
    {
        $this->combustionTech = $combustionTech;

        return $this;
    }

    public function isImpulseMotorTech(): ?bool
    {
        return $this->impulseMotorTech;
    }

    public function setImpulseMotorTech(bool $impulseMotorTech): self
    {
        $this->impulseMotorTech = $impulseMotorTech;

        return $this;
    }

    public function isHyperspaceMotorTech(): ?bool
    {
        return $this->hyperspaceMotorTech;
    }

    public function setHyperspaceMotorTech(bool $hyperspaceMotorTech): self
    {
        $this->hyperspaceMotorTech = $hyperspaceMotorTech;

        return $this;
    }

    public function isLaserTech(): ?bool
    {
        return $this->laserTech;
    }

    public function setLaserTech(bool $laserTech): self
    {
        $this->laserTech = $laserTech;

        return $this;
    }

    public function isIonicTech(): ?bool
    {
        return $this->ionicTech;
    }

    public function setIonicTech(bool $ionicTech): self
    {
        $this->ionicTech = $ionicTech;

        return $this;
    }

    public function isBusterTech(): ?bool
    {
        return $this->busterTech;
    }

    public function setBusterTech(bool $busterTech): self
    {
        $this->busterTech = $busterTech;

        return $this;
    }

    public function isIntergalacticTech(): ?bool
    {
        return $this->intergalacticTech;
    }

    public function setIntergalacticTech(bool $intergalacticTech): self
    {
        $this->intergalacticTech = $intergalacticTech;

        return $this;
    }

    public function isExpeditionTech(): ?bool
    {
        return $this->expeditionTech;
    }

    public function setExpeditionTech(bool $expeditionTech): self
    {
        $this->expeditionTech = $expeditionTech;

        return $this;
    }

    public function isMetalProcTech(): ?bool
    {
        return $this->metalProcTech;
    }

    public function setMetalProcTech(bool $metalProcTech): self
    {
        $this->metalProcTech = $metalProcTech;

        return $this;
    }

    public function isCrystalProcTech(): ?bool
    {
        return $this->crystalProcTech;
    }

    public function setCrystalProcTech(bool $crystalProcTech): self
    {
        $this->crystalProcTech = $crystalProcTech;

        return $this;
    }

    public function isDeuteriumProcTech(): ?bool
    {
        return $this->deuteriumProcTech;
    }

    public function setDeuteriumProcTech(bool $deuteriumProcTech): self
    {
        $this->deuteriumProcTech = $deuteriumProcTech;

        return $this;
    }

    public function isGravitonTech(): ?bool
    {
        return $this->gravitonTech;
    }

    public function setGravitonTech(bool $gravitonTech): self
    {
        $this->gravitonTech = $gravitonTech;

        return $this;
    }

    public function getAllyId(): ?int
    {
        return $this->allyId;
    }

    public function setAllyId(int $allyId): self
    {
        $this->allyId = $allyId;

        return $this;
    }

    public function getAllyRegisterTime(): ?int
    {
        return $this->allyRegisterTime;
    }

    public function setAllyRegisterTime(int $allyRegisterTime): self
    {
        $this->allyRegisterTime = $allyRegisterTime;

        return $this;
    }

    public function getAllyRankId(): ?int
    {
        return $this->allyRankId;
    }

    public function setAllyRankId(int $allyRankId): self
    {
        $this->allyRankId = $allyRankId;

        return $this;
    }

    public function isBana(): ?bool
    {
        return $this->bana;
    }

    public function setBana(bool $bana): self
    {
        $this->bana = $bana;

        return $this;
    }

    public function getBanaday(): ?int
    {
        return $this->banaday;
    }

    public function setBanaday(int $banaday): self
    {
        $this->banaday = $banaday;

        return $this;
    }

    public function isHof(): ?bool
    {
        return $this->hof;
    }

    public function setHof(bool $hof): self
    {
        $this->hof = $hof;

        return $this;
    }

    public function isSpymessagesmode(): ?bool
    {
        return $this->spymessagesmode;
    }

    public function setSpymessagesmode(bool $spymessagesmode): self
    {
        $this->spymessagesmode = $spymessagesmode;

        return $this;
    }

    public function getWons(): ?int
    {
        return $this->wons;
    }

    public function setWons(int $wons): self
    {
        $this->wons = $wons;

        return $this;
    }

    public function getLoos(): ?int
    {
        return $this->loos;
    }

    public function setLoos(int $loos): self
    {
        $this->loos = $loos;

        return $this;
    }

    public function getDraws(): ?int
    {
        return $this->draws;
    }

    public function setDraws(int $draws): self
    {
        $this->draws = $draws;

        return $this;
    }

    public function getKbmetal(): ?float
    {
        return $this->kbmetal;
    }

    public function setKbmetal(float $kbmetal): self
    {
        $this->kbmetal = $kbmetal;

        return $this;
    }

    public function getKbcrystal(): ?float
    {
        return $this->kbcrystal;
    }

    public function setKbcrystal(float $kbcrystal): self
    {
        $this->kbcrystal = $kbcrystal;

        return $this;
    }

    public function getLostunits(): ?float
    {
        return $this->lostunits;
    }

    public function setLostunits(float $lostunits): self
    {
        $this->lostunits = $lostunits;

        return $this;
    }

    public function getDesunits(): ?float
    {
        return $this->desunits;
    }

    public function setDesunits(float $desunits): self
    {
        $this->desunits = $desunits;

        return $this;
    }

    public function getUctime(): ?int
    {
        return $this->uctime;
    }

    public function setUctime(int $uctime): self
    {
        $this->uctime = $uctime;

        return $this;
    }

    public function getSetmail(): ?int
    {
        return $this->setmail;
    }

    public function setSetmail(int $setmail): self
    {
        $this->setmail = $setmail;

        return $this;
    }

    public function getRefId(): ?int
    {
        return $this->refId;
    }

    public function setRefId(int $refId): self
    {
        $this->refId = $refId;

        return $this;
    }

    public function isRefBonus(): ?bool
    {
        return $this->refBonus;
    }

    public function setRefBonus(bool $refBonus): self
    {
        $this->refBonus = $refBonus;

        return $this;
    }

    public function isInactiveMail(): ?bool
    {
        return $this->inactiveMail;
    }

    public function setInactiveMail(bool $inactiveMail): self
    {
        $this->inactiveMail = $inactiveMail;

        return $this;
    }

    public function getColormission2friend(): ?string
    {
        return $this->colormission2friend;
    }

    public function setColormission2friend(string $colormission2friend): self
    {
        $this->colormission2friend = $colormission2friend;

        return $this;
    }

    public function getColormission1own(): ?string
    {
        return $this->colormission1own;
    }

    public function setColormission1own(string $colormission1own): self
    {
        $this->colormission1own = $colormission1own;

        return $this;
    }

    public function getColormission2own(): ?string
    {
        return $this->colormission2own;
    }

    public function setColormission2own(string $colormission2own): self
    {
        $this->colormission2own = $colormission2own;

        return $this;
    }

    public function getColormission3own(): ?string
    {
        return $this->colormission3own;
    }

    public function setColormission3own(string $colormission3own): self
    {
        $this->colormission3own = $colormission3own;

        return $this;
    }

    public function getColormission4own(): ?string
    {
        return $this->colormission4own;
    }

    public function setColormission4own(string $colormission4own): self
    {
        $this->colormission4own = $colormission4own;

        return $this;
    }

    public function getColormission5own(): ?string
    {
        return $this->colormission5own;
    }

    public function setColormission5own(string $colormission5own): self
    {
        $this->colormission5own = $colormission5own;

        return $this;
    }

    public function getColormission6own(): ?string
    {
        return $this->colormission6own;
    }

    public function setColormission6own(string $colormission6own): self
    {
        $this->colormission6own = $colormission6own;

        return $this;
    }

    public function getColormission7own(): ?string
    {
        return $this->colormission7own;
    }

    public function setColormission7own(string $colormission7own): self
    {
        $this->colormission7own = $colormission7own;

        return $this;
    }

    public function getColormission7ownreturn(): ?string
    {
        return $this->colormission7ownreturn;
    }

    public function setColormission7ownreturn(string $colormission7ownreturn): self
    {
        $this->colormission7ownreturn = $colormission7ownreturn;

        return $this;
    }

    public function getColormission8own(): ?string
    {
        return $this->colormission8own;
    }

    public function setColormission8own(string $colormission8own): self
    {
        $this->colormission8own = $colormission8own;

        return $this;
    }

    public function getColormission9own(): ?string
    {
        return $this->colormission9own;
    }

    public function setColormission9own(string $colormission9own): self
    {
        $this->colormission9own = $colormission9own;

        return $this;
    }

    public function getColormission10own(): ?string
    {
        return $this->colormission10own;
    }

    public function setColormission10own(string $colormission10own): self
    {
        $this->colormission10own = $colormission10own;

        return $this;
    }

    public function getColormission15own(): ?string
    {
        return $this->colormission15own;
    }

    public function setColormission15own(string $colormission15own): self
    {
        $this->colormission15own = $colormission15own;

        return $this;
    }

    public function getColormission16own(): ?string
    {
        return $this->colormission16own;
    }

    public function setColormission16own(string $colormission16own): self
    {
        $this->colormission16own = $colormission16own;

        return $this;
    }

    public function getColormission17own(): ?string
    {
        return $this->colormission17own;
    }

    public function setColormission17own(string $colormission17own): self
    {
        $this->colormission17own = $colormission17own;

        return $this;
    }

    public function getColormissionreturnown(): ?string
    {
        return $this->colormissionreturnown;
    }

    public function setColormissionreturnown(string $colormissionreturnown): self
    {
        $this->colormissionreturnown = $colormissionreturnown;

        return $this;
    }

    public function getColormission1foreign(): ?string
    {
        return $this->colormission1foreign;
    }

    public function setColormission1foreign(string $colormission1foreign): self
    {
        $this->colormission1foreign = $colormission1foreign;

        return $this;
    }

    public function getColormission2foreign(): ?string
    {
        return $this->colormission2foreign;
    }

    public function setColormission2foreign(string $colormission2foreign): self
    {
        $this->colormission2foreign = $colormission2foreign;

        return $this;
    }

    public function getColormission3foreign(): ?string
    {
        return $this->colormission3foreign;
    }

    public function setColormission3foreign(string $colormission3foreign): self
    {
        $this->colormission3foreign = $colormission3foreign;

        return $this;
    }

    public function getColormission4foreign(): ?string
    {
        return $this->colormission4foreign;
    }

    public function setColormission4foreign(string $colormission4foreign): self
    {
        $this->colormission4foreign = $colormission4foreign;

        return $this;
    }

    public function getColormission5foreign(): ?string
    {
        return $this->colormission5foreign;
    }

    public function setColormission5foreign(string $colormission5foreign): self
    {
        $this->colormission5foreign = $colormission5foreign;

        return $this;
    }

    public function getColormission6foreign(): ?string
    {
        return $this->colormission6foreign;
    }

    public function setColormission6foreign(string $colormission6foreign): self
    {
        $this->colormission6foreign = $colormission6foreign;

        return $this;
    }

    public function getColormission7foreign(): ?string
    {
        return $this->colormission7foreign;
    }

    public function setColormission7foreign(string $colormission7foreign): self
    {
        $this->colormission7foreign = $colormission7foreign;

        return $this;
    }

    public function getColormission8foreign(): ?string
    {
        return $this->colormission8foreign;
    }

    public function setColormission8foreign(string $colormission8foreign): self
    {
        $this->colormission8foreign = $colormission8foreign;

        return $this;
    }

    public function getColormission9foreign(): ?string
    {
        return $this->colormission9foreign;
    }

    public function setColormission9foreign(string $colormission9foreign): self
    {
        $this->colormission9foreign = $colormission9foreign;

        return $this;
    }

    public function getColormission10foreign(): ?string
    {
        return $this->colormission10foreign;
    }

    public function setColormission10foreign(string $colormission10foreign): self
    {
        $this->colormission10foreign = $colormission10foreign;

        return $this;
    }

    public function getColormission15foreign(): ?string
    {
        return $this->colormission15foreign;
    }

    public function setColormission15foreign(string $colormission15foreign): self
    {
        $this->colormission15foreign = $colormission15foreign;

        return $this;
    }

    public function getColormission16foreign(): ?string
    {
        return $this->colormission16foreign;
    }

    public function setColormission16foreign(string $colormission16foreign): self
    {
        $this->colormission16foreign = $colormission16foreign;

        return $this;
    }

    public function getColormission17foreign(): ?string
    {
        return $this->colormission17foreign;
    }

    public function setColormission17foreign(string $colormission17foreign): self
    {
        $this->colormission17foreign = $colormission17foreign;

        return $this;
    }

    public function getColormissionreturnforeign(): ?string
    {
        return $this->colormissionreturnforeign;
    }

    public function setColormissionreturnforeign(string $colormissionreturnforeign): self
    {
        $this->colormissionreturnforeign = $colormissionreturnforeign;

        return $this;
    }

    public function getColorstatictimer(): ?string
    {
        return $this->colorstatictimer;
    }

    public function setColorstatictimer(string $colorstatictimer): self
    {
        $this->colorstatictimer = $colorstatictimer;

        return $this;
    }

    public function getColorpositive(): ?string
    {
        return $this->colorpositive;
    }

    public function setColorpositive(string $colorpositive): self
    {
        $this->colorpositive = $colorpositive;

        return $this;
    }

    public function getColornegative(): ?string
    {
        return $this->colornegative;
    }

    public function setColornegative(string $colornegative): self
    {
        $this->colornegative = $colornegative;

        return $this;
    }

    public function getColorneutral(): ?string
    {
        return $this->colorneutral;
    }

    public function setColorneutral(string $colorneutral): self
    {
        $this->colorneutral = $colorneutral;

        return $this;
    }

    public function isPriomission1(): ?bool
    {
        return $this->priomission1;
    }

    public function setPriomission1(bool $priomission1): self
    {
        $this->priomission1 = $priomission1;

        return $this;
    }

    public function isPriomission2(): ?bool
    {
        return $this->priomission2;
    }

    public function setPriomission2(bool $priomission2): self
    {
        $this->priomission2 = $priomission2;

        return $this;
    }

    public function isPriomission3(): ?bool
    {
        return $this->priomission3;
    }

    public function setPriomission3(bool $priomission3): self
    {
        $this->priomission3 = $priomission3;

        return $this;
    }

    public function isPriomission4(): ?bool
    {
        return $this->priomission4;
    }

    public function setPriomission4(bool $priomission4): self
    {
        $this->priomission4 = $priomission4;

        return $this;
    }

    public function isPriomission5(): ?bool
    {
        return $this->priomission5;
    }

    public function setPriomission5(bool $priomission5): self
    {
        $this->priomission5 = $priomission5;

        return $this;
    }

    public function isPriomission6(): ?bool
    {
        return $this->priomission6;
    }

    public function setPriomission6(bool $priomission6): self
    {
        $this->priomission6 = $priomission6;

        return $this;
    }

    public function isPriomission7(): ?bool
    {
        return $this->priomission7;
    }

    public function setPriomission7(bool $priomission7): self
    {
        $this->priomission7 = $priomission7;

        return $this;
    }

    public function isPriomission8(): ?bool
    {
        return $this->priomission8;
    }

    public function setPriomission8(bool $priomission8): self
    {
        $this->priomission8 = $priomission8;

        return $this;
    }

    public function isPriomission9(): ?bool
    {
        return $this->priomission9;
    }

    public function setPriomission9(bool $priomission9): self
    {
        $this->priomission9 = $priomission9;

        return $this;
    }

    public function isPriomission17(): ?bool
    {
        return $this->priomission17;
    }

    public function setPriomission17(bool $priomission17): self
    {
        $this->priomission17 = $priomission17;

        return $this;
    }

    public function getStbSmallRess(): ?int
    {
        return $this->stbSmallRess;
    }

    public function setStbSmallRess(int $stbSmallRess): self
    {
        $this->stbSmallRess = $stbSmallRess;

        return $this;
    }

    public function getStbMedRess(): ?int
    {
        return $this->stbMedRess;
    }

    public function setStbMedRess(int $stbMedRess): self
    {
        $this->stbMedRess = $stbMedRess;

        return $this;
    }

    public function getStbBigRess(): ?int
    {
        return $this->stbBigRess;
    }

    public function setStbBigRess(int $stbBigRess): self
    {
        $this->stbBigRess = $stbBigRess;

        return $this;
    }

    public function isStbSmallTime(): ?bool
    {
        return $this->stbSmallTime;
    }

    public function setStbSmallTime(bool $stbSmallTime): self
    {
        $this->stbSmallTime = $stbSmallTime;

        return $this;
    }

    public function isStbMedTime(): ?bool
    {
        return $this->stbMedTime;
    }

    public function setStbMedTime(bool $stbMedTime): self
    {
        $this->stbMedTime = $stbMedTime;

        return $this;
    }

    public function isStbBigTime(): ?bool
    {
        return $this->stbBigTime;
    }

    public function setStbBigTime(bool $stbBigTime): self
    {
        $this->stbBigTime = $stbBigTime;

        return $this;
    }

    public function isStbEnabled(): ?bool
    {
        return $this->stbEnabled;
    }

    public function setStbEnabled(bool $stbEnabled): self
    {
        $this->stbEnabled = $stbEnabled;

        return $this;
    }

    public function isRecordsOptin(): ?bool
    {
        return $this->recordsOptin;
    }

    public function setRecordsOptin(bool $recordsOptin): self
    {
        $this->recordsOptin = $recordsOptin;

        return $this;
    }


    public function getRoles(): array
    {
        $roles = [];
        match ($this->authLevel) {
            AuthLevel::Admin => $roles[] = 'ROLE_ADMIN',
            AuthLevel::Operator => $roles[] = 'ROLE_OPERATOR',
            AuthLevel::Moderator => $roles[] = 'ROLE_MODERATOR',
            AuthLevel::User => $roles[] = 'ROLE_USER',
        };
        return $roles;
    }

    public function eraseCredentials()
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }
}
