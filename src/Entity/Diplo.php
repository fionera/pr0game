<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diplo
 */
#[ORM\Table(name: 'diplo')]
#[ORM\Index(name: 'owner_1', columns: ['owner_1', 'owner_2', 'accept'])]
#[ORM\Index(name: 'universe', columns: ['universe'])]
#[ORM\Entity]
class Diplo
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'owner_1', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $owner1;

    /**
     * @var int
     */
    #[ORM\Column(name: 'owner_2', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $owner2;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'level', type: 'boolean', nullable: false)]
    private $level;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'accept', type: 'boolean', nullable: false)]
    private $accept;

    /**
     * @var string
     */
    #[ORM\Column(name: 'accept_text', type: 'string', length: 255, nullable: false)]
    private $acceptText;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;

    #[ORM\Column(name: 'request_time', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $requestTime = '0';


}
