<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vars
 */
#[ORM\Table(name: 'vars')]
#[ORM\Index(name: 'class', columns: ['class'])]
#[ORM\Entity]
class Vars
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'elementID', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $elementid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 32, nullable: false)]
    private $name;

    /**
     * @var int
     */
    #[ORM\Column(name: 'class', type: 'integer', nullable: false)]
    private $class;

    /**
     * @var array
     */
    #[ORM\Column(name: 'onPlanetType', type: 'simple_array', length: 0, nullable: false)]
    private $onplanettype;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'onePerPlanet', type: 'boolean', nullable: false)]
    private $oneperplanet;

    /**
     * @var float
     */
    #[ORM\Column(name: 'factor', type: 'float', precision: 4, scale: 2, nullable: false)]
    private $factor;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'maxLevel', type: 'integer', nullable: true)]
    private $maxlevel;

    #[ORM\Column(name: 'cost901', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $cost901 = '0';

    #[ORM\Column(name: 'cost902', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $cost902 = '0';

    #[ORM\Column(name: 'cost903', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $cost903 = '0';

    #[ORM\Column(name: 'cost911', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $cost911 = '0';

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'consumption1', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $consumption1;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'consumption2', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $consumption2;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'speedTech', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $speedtech;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'speed1', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $speed1;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'speed2', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $speed2;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'speed3', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $speed3;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'capacity', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $capacity;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'attack', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $attack;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'defend', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $defend;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'timeBonus', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $timebonus;

    #[ORM\Column(name: 'bonusAttack', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusattack = 0.00;

    #[ORM\Column(name: 'bonusDefensive', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusdefensive = 0.00;

    #[ORM\Column(name: 'bonusShield', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusshield = 0.00;

    #[ORM\Column(name: 'bonusBuildTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusbuildtime = 0.00;

    #[ORM\Column(name: 'bonusResearchTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusresearchtime = 0.00;

    #[ORM\Column(name: 'bonusShipTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusshiptime = 0.00;

    #[ORM\Column(name: 'bonusDefensiveTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusdefensivetime = 0.00;

    #[ORM\Column(name: 'bonusResource', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusresource = 0.00;

    #[ORM\Column(name: 'bonusEnergy', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusenergy = 0.00;

    #[ORM\Column(name: 'bonusResourceStorage', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusresourcestorage = 0.00;

    #[ORM\Column(name: 'bonusShipStorage', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusshipstorage = 0.00;

    #[ORM\Column(name: 'bonusFlyTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusflytime = 0.00;

    #[ORM\Column(name: 'bonusFleetSlots', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusfleetslots = 0.00;

    #[ORM\Column(name: 'bonusPlanets', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusplanets = 0.00;

    #[ORM\Column(name: 'bonusSpyPower', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusspypower = 0.00;

    #[ORM\Column(name: 'bonusExpedition', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusexpedition = 0.00;

    #[ORM\Column(name: 'bonusGateCoolTime', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusgatecooltime = 0.00;

    #[ORM\Column(name: 'bonusMoreFound', type: 'float', precision: 4, scale: 2, nullable: false, options: ['default' => '0.00'])]
    private float $bonusmorefound = 0.00;

    #[ORM\Column(name: 'bonusAttackUnit', type: 'smallint', nullable: false)]
    private string $bonusattackunit = '0';

    #[ORM\Column(name: 'bonusDefensiveUnit', type: 'smallint', nullable: false)]
    private string $bonusdefensiveunit = '0';

    #[ORM\Column(name: 'bonusShieldUnit', type: 'smallint', nullable: false)]
    private string $bonusshieldunit = '0';

    #[ORM\Column(name: 'bonusBuildTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusbuildtimeunit = '0';

    #[ORM\Column(name: 'bonusResearchTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusresearchtimeunit = '0';

    #[ORM\Column(name: 'bonusShipTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusshiptimeunit = '0';

    #[ORM\Column(name: 'bonusDefensiveTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusdefensivetimeunit = '0';

    #[ORM\Column(name: 'bonusResourceUnit', type: 'smallint', nullable: false)]
    private string $bonusresourceunit = '0';

    #[ORM\Column(name: 'bonusEnergyUnit', type: 'smallint', nullable: false)]
    private string $bonusenergyunit = '0';

    #[ORM\Column(name: 'bonusResourceStorageUnit', type: 'smallint', nullable: false)]
    private string $bonusresourcestorageunit = '0';

    #[ORM\Column(name: 'bonusShipStorageUnit', type: 'smallint', nullable: false)]
    private string $bonusshipstorageunit = '0';

    #[ORM\Column(name: 'bonusFlyTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusflytimeunit = '0';

    #[ORM\Column(name: 'bonusFleetSlotsUnit', type: 'smallint', nullable: false)]
    private string $bonusfleetslotsunit = '0';

    #[ORM\Column(name: 'bonusPlanetsUnit', type: 'smallint', nullable: false)]
    private string $bonusplanetsunit = '0';

    #[ORM\Column(name: 'bonusSpyPowerUnit', type: 'smallint', nullable: false)]
    private string $bonusspypowerunit = '0';

    #[ORM\Column(name: 'bonusExpeditionUnit', type: 'smallint', nullable: false)]
    private string $bonusexpeditionunit = '0';

    #[ORM\Column(name: 'bonusGateCoolTimeUnit', type: 'smallint', nullable: false)]
    private string $bonusgatecooltimeunit = '0';

    #[ORM\Column(name: 'bonusMoreFoundUnit', type: 'smallint', nullable: false)]
    private string $bonusmorefoundunit = '0';

    /**
     * @var float|null
     */
    #[ORM\Column(name: 'speedFleetFactor', type: 'float', precision: 4, scale: 2, nullable: true)]
    private $speedfleetfactor;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'production901', type: 'string', length: 255, nullable: true)]
    private $production901;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'production902', type: 'string', length: 255, nullable: true)]
    private $production902;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'production903', type: 'string', length: 255, nullable: true)]
    private $production903;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'production911', type: 'string', length: 255, nullable: true)]
    private $production911;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'storage901', type: 'string', length: 255, nullable: true)]
    private $storage901;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'storage902', type: 'string', length: 255, nullable: true)]
    private $storage902;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'storage903', type: 'string', length: 255, nullable: true)]
    private $storage903;


}
