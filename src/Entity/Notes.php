<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notes
 */
#[ORM\Table(name: 'notes')]
#[ORM\Index(name: 'owner', columns: ['owner', 'time', 'priority'])]
#[ORM\Index(name: 'universe', columns: ['universe'])]
#[ORM\Entity]
class Notes
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'owner', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $owner;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: true)]
    private $time;

    /**
     * @var bool|null
     */
    #[ORM\Column(name: 'priority', type: 'boolean', nullable: true, options: ['default' => 1])]
    private bool $priority = true;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'title', type: 'string', length: 32, nullable: true)]
    private $title;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'text', type: 'text', length: 65535, nullable: true)]
    private $text;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;


}
