<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Buddy
 */
#[ORM\Table(name: 'buddy')]
#[ORM\Index(name: 'sender', columns: ['sender', 'owner'])]
#[ORM\Index(name: 'universe', columns: ['universe'])]
#[ORM\Entity]
class Buddy
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    #[ORM\Column(name: 'sender', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $sender = '0';

    #[ORM\Column(name: 'owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $owner = '0';

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;


}
