<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Records
 */
#[ORM\Table(name: 'records')]
#[ORM\Entity]
class Records
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'elementID', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private $elementid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'level', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private $level;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;


}
