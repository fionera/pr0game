<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VarsRapidfire
 */
#[ORM\Table(name: 'vars_rapidfire')]
#[ORM\Index(name: 'elementID', columns: ['elementID'])]
#[ORM\Index(name: 'rapidfireID', columns: ['rapidfireID'])]
#[ORM\Entity]
class VarsRapidfire
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'elementID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $elementid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'rapidfireID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $rapidfireid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'shoots', type: 'integer', nullable: false)]
    private $shoots;


}
