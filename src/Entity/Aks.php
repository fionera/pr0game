<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aks
 */
#[ORM\Table(name: 'aks')]
#[ORM\Entity]
class Aks
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'name', type: 'string', length: 50, nullable: true)]
    private $name;

    /**
     * @var int
     */
    #[ORM\Column(name: 'target', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $target;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'ankunft', type: 'integer', nullable: true)]
    private $ankunft;


}
