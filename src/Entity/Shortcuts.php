<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shortcuts
 */
#[ORM\Table(name: 'shortcuts')]
#[ORM\Index(name: 'ownerID', columns: ['ownerID'])]
#[ORM\Entity]
class Shortcuts
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'shortcutID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $shortcutid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'ownerID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $ownerid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 32, nullable: false)]
    private $name;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'galaxy', type: 'boolean', nullable: false)]
    private $galaxy;

    /**
     * @var int
     */
    #[ORM\Column(name: 'system', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private $system;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'planet', type: 'boolean', nullable: false)]
    private $planet;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'type', type: 'boolean', nullable: false)]
    private $type;


}
