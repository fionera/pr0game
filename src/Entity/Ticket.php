<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 */
#[ORM\Table(name: 'ticket')]
#[ORM\Index(name: 'ownerID', columns: ['ownerID'])]
#[ORM\Index(name: 'universe', columns: ['universe', 'status'])]
#[ORM\Entity]
class Ticket
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'ticketID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $ticketid;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;

    /**
     * @var int
     */
    #[ORM\Column(name: 'ownerID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $ownerid;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'categoryID', type: 'boolean', nullable: false)]
    private $categoryid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'subject', type: 'string', length: 255, nullable: false)]
    private $subject;

    #[ORM\Column(name: 'status', type: 'boolean', nullable: false)]
    private string $status = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $time;


}
