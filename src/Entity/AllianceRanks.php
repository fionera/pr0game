<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AllianceRanks
 */
#[ORM\Table(name: 'alliance_ranks')]
#[ORM\Index(name: 'allianceID', columns: ['allianceID', 'rankID'])]
#[ORM\Entity]
class AllianceRanks
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'rankID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $rankid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'rankName', type: 'string', length: 32, nullable: false)]
    private $rankname;

    /**
     * @var int
     */
    #[ORM\Column(name: 'allianceID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $allianceid;

    #[ORM\Column(name: 'MEMBERLIST', type: 'boolean', nullable: false)]
    private string $memberlist = '0';

    #[ORM\Column(name: 'ONLINESTATE', type: 'boolean', nullable: false)]
    private string $onlinestate = '0';

    #[ORM\Column(name: 'TRANSFER', type: 'boolean', nullable: false)]
    private string $transfer = '0';

    #[ORM\Column(name: 'SEEAPPLY', type: 'boolean', nullable: false)]
    private string $seeapply = '0';

    #[ORM\Column(name: 'MANAGEAPPLY', type: 'boolean', nullable: false)]
    private string $manageapply = '0';

    #[ORM\Column(name: 'ROUNDMAIL', type: 'boolean', nullable: false)]
    private string $roundmail = '0';

    #[ORM\Column(name: 'ADMIN', type: 'boolean', nullable: false)]
    private string $admin = '0';

    #[ORM\Column(name: 'KICK', type: 'boolean', nullable: false)]
    private string $kick = '0';

    #[ORM\Column(name: 'DIPLOMATIC', type: 'boolean', nullable: false)]
    private string $diplomatic = '0';

    #[ORM\Column(name: 'RANKS', type: 'boolean', nullable: false)]
    private string $ranks = '0';

    #[ORM\Column(name: 'MANAGEUSERS', type: 'boolean', nullable: false)]
    private string $manageusers = '0';

    #[ORM\Column(name: 'EVENTS', type: 'boolean', nullable: false)]
    private string $events = '0';


}
