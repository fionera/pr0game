<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BuddyRequest
 */
#[ORM\Table(name: 'buddy_request')]
#[ORM\Entity]
class BuddyRequest
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'text', type: 'text', length: 65535, nullable: false)]
    private $text;


}
