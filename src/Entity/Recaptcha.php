<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recaptcha
 */
#[ORM\Table(name: 'recaptcha')]
#[ORM\Entity]
class Recaptcha
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userId', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    #[ORM\Column(name: 'success', type: 'boolean', nullable: false)]
    private string $success = '0';

    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private string $time = '0';

    #[ORM\Column(name: 'score', type: 'decimal', precision: 2, scale: 1, nullable: false, options: ['default' => '0.0'])]
    private string $score = '0.0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'action', type: 'string', length: 255, nullable: true)]
    private string $action = '';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'url', type: 'string', length: 255, nullable: true)]
    private $url;


}
