<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersValid
 */
#[ORM\Table(name: 'users_valid')]
#[ORM\Entity]
class UsersValid
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'validationID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $validationid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'userName', type: 'string', length: 64, nullable: false)]
    private $username;

    /**
     * @var string
     */
    #[ORM\Column(name: 'validationKey', type: 'string', length: 32, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $validationkey;

    /**
     * @var string
     */
    #[ORM\Column(name: 'password', type: 'string', length: 60, nullable: false)]
    private $password;

    /**
     * @var string
     */
    #[ORM\Column(name: 'email', type: 'string', length: 64, nullable: false)]
    private $email;

    /**
     * @var int
     */
    #[ORM\Column(name: 'date', type: 'integer', nullable: false)]
    private $date;

    /**
     * @var string
     */
    #[ORM\Column(name: 'ip', type: 'string', length: 40, nullable: false)]
    private $ip;

    /**
     * @var string
     */
    #[ORM\Column(name: 'language', type: 'string', length: 3, nullable: false)]
    private $language;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'referralID', type: 'integer', nullable: true)]
    private $referralid;


}
