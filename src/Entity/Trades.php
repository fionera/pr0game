<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trades
 */
#[ORM\Table(name: 'trades')]
#[ORM\Entity]
class Trades
{
    #[ORM\Column(name: 'seller_fleet_id', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private string $sellerFleetId = '0';

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'buyer_fleet_id', type: 'bigint', nullable: true, options: ['unsigned' => true])]
    private $buyerFleetId;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'buy_time', type: 'datetime', nullable: true)]
    private $buyTime;

    #[ORM\Column(name: 'transaction_type', type: 'boolean', nullable: false)]
    private string $transactionType = '0';

    #[ORM\Column(name: 'filter_visibility', type: 'boolean', nullable: false)]
    private string $filterVisibility = '0';

    #[ORM\Column(name: 'filter_flighttime', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $filterFlighttime = '0';

    #[ORM\Column(name: 'ex_resource_type', type: 'boolean', nullable: false)]
    private string $exResourceType = '0';

    #[ORM\Column(name: 'ex_resource_amount', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $exResourceAmount = '0';


}
