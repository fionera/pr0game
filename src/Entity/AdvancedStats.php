<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdvancedStats
 */
#[ORM\Table(name: 'advanced_stats')]
#[ORM\Entity]
class AdvancedStats
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'userId', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $userid;

    #[ORM\Column(name: 'build_202', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build202 = '0';

    #[ORM\Column(name: 'build_203', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build203 = '0';

    #[ORM\Column(name: 'build_204', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build204 = '0';

    #[ORM\Column(name: 'build_205', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build205 = '0';

    #[ORM\Column(name: 'build_206', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build206 = '0';

    #[ORM\Column(name: 'build_207', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build207 = '0';

    #[ORM\Column(name: 'build_208', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build208 = '0';

    #[ORM\Column(name: 'build_209', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build209 = '0';

    #[ORM\Column(name: 'build_210', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build210 = '0';

    #[ORM\Column(name: 'build_211', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build211 = '0';

    #[ORM\Column(name: 'build_212', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build212 = '0';

    #[ORM\Column(name: 'build_213', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build213 = '0';

    #[ORM\Column(name: 'build_214', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build214 = '0';

    #[ORM\Column(name: 'build_215', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build215 = '0';

    #[ORM\Column(name: 'build_216', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build216 = '0';

    #[ORM\Column(name: 'build_217', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build217 = '0';

    #[ORM\Column(name: 'build_218', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build218 = '0';

    #[ORM\Column(name: 'build_219', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build219 = '0';

    #[ORM\Column(name: 'build_401', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build401 = '0';

    #[ORM\Column(name: 'build_402', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build402 = '0';

    #[ORM\Column(name: 'build_403', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build403 = '0';

    #[ORM\Column(name: 'build_404', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build404 = '0';

    #[ORM\Column(name: 'build_405', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build405 = '0';

    #[ORM\Column(name: 'build_406', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build406 = '0';

    #[ORM\Column(name: 'build_407', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build407 = '0';

    #[ORM\Column(name: 'build_408', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build408 = '0';

    #[ORM\Column(name: 'build_409', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build409 = '0';

    #[ORM\Column(name: 'build_410', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build410 = '0';

    #[ORM\Column(name: 'build_411', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build411 = '0';

    #[ORM\Column(name: 'build_502', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build502 = '0';

    #[ORM\Column(name: 'build_503', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $build503 = '0';

    #[ORM\Column(name: 'lost_202', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost202 = '0';

    #[ORM\Column(name: 'lost_203', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost203 = '0';

    #[ORM\Column(name: 'lost_204', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost204 = '0';

    #[ORM\Column(name: 'lost_205', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost205 = '0';

    #[ORM\Column(name: 'lost_206', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost206 = '0';

    #[ORM\Column(name: 'lost_207', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost207 = '0';

    #[ORM\Column(name: 'lost_208', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost208 = '0';

    #[ORM\Column(name: 'lost_209', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost209 = '0';

    #[ORM\Column(name: 'lost_210', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost210 = '0';

    #[ORM\Column(name: 'lost_211', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost211 = '0';

    #[ORM\Column(name: 'lost_212', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost212 = '0';

    #[ORM\Column(name: 'lost_213', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost213 = '0';

    #[ORM\Column(name: 'lost_214', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost214 = '0';

    #[ORM\Column(name: 'lost_215', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost215 = '0';

    #[ORM\Column(name: 'lost_216', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost216 = '0';

    #[ORM\Column(name: 'lost_217', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost217 = '0';

    #[ORM\Column(name: 'lost_218', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost218 = '0';

    #[ORM\Column(name: 'lost_219', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost219 = '0';

    #[ORM\Column(name: 'lost_401', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost401 = '0';

    #[ORM\Column(name: 'lost_402', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost402 = '0';

    #[ORM\Column(name: 'lost_403', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost403 = '0';

    #[ORM\Column(name: 'lost_404', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost404 = '0';

    #[ORM\Column(name: 'lost_405', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost405 = '0';

    #[ORM\Column(name: 'lost_406', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost406 = '0';

    #[ORM\Column(name: 'lost_407', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost407 = '0';

    #[ORM\Column(name: 'lost_408', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost408 = '0';

    #[ORM\Column(name: 'lost_409', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost409 = '0';

    #[ORM\Column(name: 'lost_410', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost410 = '0';

    #[ORM\Column(name: 'lost_411', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost411 = '0';

    #[ORM\Column(name: 'lost_502', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost502 = '0';

    #[ORM\Column(name: 'lost_503', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $lost503 = '0';

    #[ORM\Column(name: 'repaired_401', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired401 = '0';

    #[ORM\Column(name: 'repaired_402', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired402 = '0';

    #[ORM\Column(name: 'repaired_403', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired403 = '0';

    #[ORM\Column(name: 'repaired_404', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired404 = '0';

    #[ORM\Column(name: 'repaired_405', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired405 = '0';

    #[ORM\Column(name: 'repaired_406', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired406 = '0';

    #[ORM\Column(name: 'repaired_407', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired407 = '0';

    #[ORM\Column(name: 'repaired_408', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired408 = '0';

    #[ORM\Column(name: 'repaired_409', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired409 = '0';

    #[ORM\Column(name: 'repaired_410', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired410 = '0';

    #[ORM\Column(name: 'repaired_411', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $repaired411 = '0';

    #[ORM\Column(name: 'destroyed_202', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed202 = '0';

    #[ORM\Column(name: 'destroyed_203', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed203 = '0';

    #[ORM\Column(name: 'destroyed_204', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed204 = '0';

    #[ORM\Column(name: 'destroyed_205', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed205 = '0';

    #[ORM\Column(name: 'destroyed_206', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed206 = '0';

    #[ORM\Column(name: 'destroyed_207', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed207 = '0';

    #[ORM\Column(name: 'destroyed_208', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed208 = '0';

    #[ORM\Column(name: 'destroyed_209', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed209 = '0';

    #[ORM\Column(name: 'destroyed_210', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed210 = '0';

    #[ORM\Column(name: 'destroyed_211', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed211 = '0';

    #[ORM\Column(name: 'destroyed_212', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed212 = '0';

    #[ORM\Column(name: 'destroyed_213', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed213 = '0';

    #[ORM\Column(name: 'destroyed_214', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed214 = '0';

    #[ORM\Column(name: 'destroyed_215', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed215 = '0';

    #[ORM\Column(name: 'destroyed_216', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed216 = '0';

    #[ORM\Column(name: 'destroyed_217', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed217 = '0';

    #[ORM\Column(name: 'destroyed_218', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed218 = '0';

    #[ORM\Column(name: 'destroyed_219', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed219 = '0';

    #[ORM\Column(name: 'destroyed_401', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed401 = '0';

    #[ORM\Column(name: 'destroyed_402', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed402 = '0';

    #[ORM\Column(name: 'destroyed_403', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed403 = '0';

    #[ORM\Column(name: 'destroyed_404', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed404 = '0';

    #[ORM\Column(name: 'destroyed_405', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed405 = '0';

    #[ORM\Column(name: 'destroyed_406', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed406 = '0';

    #[ORM\Column(name: 'destroyed_407', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed407 = '0';

    #[ORM\Column(name: 'destroyed_408', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed408 = '0';

    #[ORM\Column(name: 'destroyed_409', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed409 = '0';

    #[ORM\Column(name: 'destroyed_410', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed410 = '0';

    #[ORM\Column(name: 'destroyed_411', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed411 = '0';

    #[ORM\Column(name: 'destroyed_502', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed502 = '0';

    #[ORM\Column(name: 'destroyed_503', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $destroyed503 = '0';

    #[ORM\Column(name: 'found_202', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found202 = '0';

    #[ORM\Column(name: 'found_203', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found203 = '0';

    #[ORM\Column(name: 'found_204', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found204 = '0';

    #[ORM\Column(name: 'found_205', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found205 = '0';

    #[ORM\Column(name: 'found_206', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found206 = '0';

    #[ORM\Column(name: 'found_207', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found207 = '0';

    #[ORM\Column(name: 'found_208', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found208 = '0';

    #[ORM\Column(name: 'found_209', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found209 = '0';

    #[ORM\Column(name: 'found_210', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found210 = '0';

    #[ORM\Column(name: 'found_211', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found211 = '0';

    #[ORM\Column(name: 'found_212', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found212 = '0';

    #[ORM\Column(name: 'found_213', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found213 = '0';

    #[ORM\Column(name: 'found_214', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found214 = '0';

    #[ORM\Column(name: 'found_215', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found215 = '0';

    #[ORM\Column(name: 'found_216', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found216 = '0';

    #[ORM\Column(name: 'found_217', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found217 = '0';

    #[ORM\Column(name: 'found_218', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found218 = '0';

    #[ORM\Column(name: 'found_219', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found219 = '0';

    #[ORM\Column(name: 'found_901', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found901 = '0';

    #[ORM\Column(name: 'found_902', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found902 = '0';

    #[ORM\Column(name: 'found_903', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $found903 = '0';


}
