<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronjobsLog
 */
#[ORM\Table(name: 'cronjobs_log')]
#[ORM\Index(name: 'cronjobId', columns: ['cronjobId', 'executionTime'])]
#[ORM\Entity]
class CronjobsLog
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'cronjobId', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $cronjobid;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'executionTime', type: 'datetime', nullable: false)]
    private $executiontime;

    /**
     * @var string
     */
    #[ORM\Column(name: 'lockToken', type: 'string', length: 32, nullable: false)]
    private $locktoken;


}
