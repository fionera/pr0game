<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Topkb
 */
#[ORM\Table(name: 'topkb')]
#[ORM\Index(name: 'time', columns: ['universe', 'rid', 'time'])]
#[ORM\Entity]
class Topkb
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'rid', type: 'string', length: 32, nullable: false)]
    private $rid;

    /**
     * @var float
     */
    #[ORM\Column(name: 'units', type: 'float', precision: 50, scale: 0, nullable: false)]
    private $units;

    /**
     * @var string
     */
    #[ORM\Column(name: 'result', type: 'string', length: 1, nullable: false)]
    private $result;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private $time;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;

    /**
     * @var bool|null
     */
    #[ORM\Column(name: 'galaxy', type: 'boolean', nullable: true)]
    private $galaxy;

    #[ORM\Column(name: 'memorial', type: 'boolean', nullable: false)]
    private string $memorial = '0';


}
