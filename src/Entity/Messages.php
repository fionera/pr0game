<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messages
 */
#[ORM\Table(name: 'messages')]
#[ORM\Index(name: 'message_deleted', columns: ['message_deleted'])]
#[ORM\Index(name: 'message_owner', columns: ['message_owner', 'message_type', 'message_unread', 'message_deleted'])]
#[ORM\Index(name: 'message_sender', columns: ['message_sender'])]
#[ORM\Entity]
class Messages
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'message_id', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $messageId;

    #[ORM\Column(name: 'message_owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $messageOwner = '0';

    #[ORM\Column(name: 'message_sender', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $messageSender = '0';

    #[ORM\Column(name: 'message_time', type: 'integer', nullable: false)]
    private string $messageTime = '0';

    #[ORM\Column(name: 'message_type', type: 'boolean', nullable: false)]
    private string $messageType = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'message_from', type: 'string', length: 128, nullable: true)]
    private $messageFrom;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'message_subject', type: 'string', length: 255, nullable: true)]
    private $messageSubject;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'message_text', type: 'text', length: 65535, nullable: true)]
    private $messageText;

    #[ORM\Column(name: 'message_unread', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $messageUnread = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'message_universe', type: 'boolean', nullable: false)]
    private $messageUniverse;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'message_deleted', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $messageDeleted;


}
