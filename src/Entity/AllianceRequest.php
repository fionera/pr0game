<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AllianceRequest
 */
#[ORM\Table(name: 'alliance_request')]
#[ORM\Index(name: 'allianceID', columns: ['allianceID', 'userID'])]
#[ORM\Entity]
class AllianceRequest
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'applyID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $applyid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'text', type: 'text', length: 65535, nullable: false)]
    private $text;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'allianceID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $allianceid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private $time;


}
