<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * System
 */
#[ORM\Table(name: 'system')]
#[ORM\Entity]
class System
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'dbVersion', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $dbversion;


}
