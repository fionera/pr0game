<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketAnswer
 */
#[ORM\Table(name: 'ticket_answer')]
#[ORM\Entity]
class TicketAnswer
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'answerID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $answerid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'ownerID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $ownerid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'ownerName', type: 'string', length: 32, nullable: false)]
    private $ownername;

    /**
     * @var int
     */
    #[ORM\Column(name: 'ticketID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $ticketid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $time;

    /**
     * @var string
     */
    #[ORM\Column(name: 'subject', type: 'string', length: 255, nullable: false)]
    private $subject;

    /**
     * @var string
     */
    #[ORM\Column(name: 'message', type: 'text', length: 16_777_215, nullable: false)]
    private $message;


}
