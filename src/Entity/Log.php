<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 */
#[ORM\Table(name: 'log')]
#[ORM\Index(name: 'mode', columns: ['mode'])]
#[ORM\Entity]
class Log
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'mode', type: 'boolean', nullable: false)]
    private $mode;

    /**
     * @var int
     */
    #[ORM\Column(name: 'admin', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $admin;

    /**
     * @var int
     */
    #[ORM\Column(name: 'target', type: 'integer', nullable: false)]
    private $target;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $time;

    /**
     * @var string
     */
    #[ORM\Column(name: 'data', type: 'text', length: 65535, nullable: false)]
    private $data;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;


}
