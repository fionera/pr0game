<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 */
#[ORM\Table(name: 'session')]
#[ORM\Entity]
class Session
{
    /**
     * @var string
     */
    #[ORM\Column(name: 'sessionID', type: 'string', length: 32, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $sessionid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'userIP', type: 'string', length: 40, nullable: false)]
    private $userip;

    /**
     * @var int
     */
    #[ORM\Column(name: 'lastonline', type: 'integer', nullable: false)]
    private $lastonline;

    #[ORM\Column(name: 'created', type: 'integer', nullable: false)]
    private string $created = '0';


}
