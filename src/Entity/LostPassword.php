<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lostpassword
 */
#[ORM\Table(name: 'lostpassword')]
#[ORM\Index(name: 'time', columns: ['time'])]
#[ORM\UniqueConstraint(name: 'userID', columns: ['userID', 'key', 'time', 'hasChanged'])]
#[ORM\Entity]
class LostPassword
{
    /**
     * @var string
     */
    #[ORM\Column(name: 'key', type: 'string', length: 32, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $key;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $time;

    #[ORM\Column(name: 'hasChanged', type: 'boolean', nullable: false)]
    private string $haschanged = '0';

    /**
     * @var string
     */
    #[ORM\Column(name: 'fromIP', type: 'string', length: 40, nullable: false)]
    private $fromip;


}
