<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Raports
 */
#[ORM\Table(name: 'raports')]
#[ORM\Index(name: 'time', columns: ['time'])]
#[ORM\Entity]
class Raports
{
    /**
     * @var string
     */
    #[ORM\Column(name: 'rid', type: 'string', length: 32, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $rid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'raport', type: 'text', length: 0, nullable: false)]
    private $raport;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private $time;

    #[ORM\Column(name: 'attacker', type: 'string', length: 255, nullable: false)]
    private string $attacker = '';

    #[ORM\Column(name: 'defender', type: 'string', length: 255, nullable: false)]
    private string $defender = '';


}
