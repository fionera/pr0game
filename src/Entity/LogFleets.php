<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogFleets
 */
#[ORM\Table(name: 'log_fleets')]
#[ORM\Index(name: 'BashRule', columns: ['fleet_owner', 'fleet_end_id', 'fleet_start_time', 'fleet_mission', 'fleet_state'])]
#[ORM\Entity]
class LogFleets
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'fleet_id', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $fleetId;

    #[ORM\Column(name: 'fleet_owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $fleetOwner = '0';

    #[ORM\Column(name: 'fleet_owner_name', type: 'string', length: 32, nullable: false)]
    private string $fleetOwnerName = '';

    #[ORM\Column(name: 'fleet_owner_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetOwnerPoints = '0';

    #[ORM\Column(name: 'fleet_mission', type: 'boolean', nullable: false, options: ['default' => 3])]
    private string $fleetMission = '3';

    #[ORM\Column(name: 'fleet_amount', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $fleetAmount = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'fleet_array', type: 'text', length: 65535, nullable: true)]
    private $fleetArray;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'fleet_universe', type: 'boolean', nullable: false)]
    private $fleetUniverse;

    #[ORM\Column(name: 'fleet_start_time', type: 'integer', nullable: false)]
    private string $fleetStartTime = '0';

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'fleet_start_time_formated', type: 'datetime', nullable: true)]
    private $fleetStartTimeFormated;

    /**
     * @var int
     */
    #[ORM\Column(name: 'fleet_start_id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $fleetStartId;

    #[ORM\Column(name: 'fleet_start_galaxy', type: 'boolean', nullable: false)]
    private string $fleetStartGalaxy = '0';

    #[ORM\Column(name: 'fleet_start_system', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private string $fleetStartSystem = '0';

    #[ORM\Column(name: 'fleet_start_planet', type: 'boolean', nullable: false)]
    private string $fleetStartPlanet = '0';

    #[ORM\Column(name: 'fleet_start_type', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $fleetStartType = true;

    #[ORM\Column(name: 'fleet_end_time', type: 'integer', nullable: false)]
    private string $fleetEndTime = '0';

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'fleet_end_time_formated', type: 'datetime', nullable: true)]
    private $fleetEndTimeFormated;

    #[ORM\Column(name: 'fleet_end_stay', type: 'integer', nullable: false)]
    private string $fleetEndStay = '0';

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'fleet_end_stay_formated', type: 'datetime', nullable: true)]
    private $fleetEndStayFormated;

    /**
     * @var int
     */
    #[ORM\Column(name: 'fleet_end_id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $fleetEndId;

    #[ORM\Column(name: 'fleet_end_galaxy', type: 'boolean', nullable: false)]
    private string $fleetEndGalaxy = '0';

    #[ORM\Column(name: 'fleet_end_system', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private string $fleetEndSystem = '0';

    #[ORM\Column(name: 'fleet_end_planet', type: 'boolean', nullable: false)]
    private string $fleetEndPlanet = '0';

    #[ORM\Column(name: 'fleet_end_type', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $fleetEndType = true;

    #[ORM\Column(name: 'fleet_target_obj', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private string $fleetTargetObj = '0';

    #[ORM\Column(name: 'fleet_resource_metal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetResourceMetal = '0';

    #[ORM\Column(name: 'fleet_resource_crystal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetResourceCrystal = '0';

    #[ORM\Column(name: 'fleet_resource_deuterium', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetResourceDeuterium = '0';

    #[ORM\Column(name: 'fleet_gained_metal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetGainedMetal = '0';

    #[ORM\Column(name: 'fleet_gained_crystal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetGainedCrystal = '0';

    #[ORM\Column(name: 'fleet_gained_deuterium', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetGainedDeuterium = '0';

    #[ORM\Column(name: 'fleet_wanted_resource', type: 'boolean', nullable: false)]
    private string $fleetWantedResource = '0';

    #[ORM\Column(name: 'fleet_wanted_resource_amount', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetWantedResourceAmount = '0';

    #[ORM\Column(name: 'fleet_no_m_return', type: 'boolean', nullable: false)]
    private string $fleetNoMReturn = '0';

    #[ORM\Column(name: 'fleet_target_owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $fleetTargetOwner = '0';

    #[ORM\Column(name: 'fleet_target_owner_name', type: 'string', length: 32, nullable: false)]
    private string $fleetTargetOwnerName = '';

    #[ORM\Column(name: 'fleet_target_owner_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetTargetOwnerPoints = '0';

    #[ORM\Column(name: 'fleet_group', type: 'string', length: 15, nullable: false)]
    private string $fleetGroup = '0';

    #[ORM\Column(name: 'fleet_mess', type: 'boolean', nullable: false)]
    private string $fleetMess = '0';

    #[ORM\Column(name: 'start_time', type: 'integer', nullable: false)]
    private string $startTime = '0';

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'start_time_formated', type: 'datetime', nullable: true)]
    private $startTimeFormated;

    #[ORM\Column(name: 'fleet_busy', type: 'boolean', nullable: false)]
    private string $fleetBusy = '0';

    #[ORM\Column(name: 'hasCanceled', type: 'boolean', nullable: false)]
    private string $hascanceled = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'fleet_start_array', type: 'text', length: 65535, nullable: true)]
    private $fleetStartArray;

    #[ORM\Column(name: 'fleet_state', type: 'boolean', nullable: false)]
    private string $fleetState = '0';


}
