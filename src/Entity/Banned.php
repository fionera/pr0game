<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banned
 */
#[ORM\Table(name: 'banned')]
#[ORM\Index(name: 'universe', columns: ['universe'])]
#[ORM\Entity]
class Banned
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    #[ORM\Column(name: 'who', type: 'string', length: 64, nullable: false)]
    private string $who = '';

    #[ORM\Column(name: 'theme', type: 'string', length: 500, nullable: false)]
    private ?string $theme = null;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private $time = '0';

    /**
     * @var int
     */
    #[ORM\Column(name: 'longer', type: 'integer', nullable: false)]
    private $longer = '0';

    #[ORM\Column(name: 'author', type: 'string', length: 64, nullable: false)]
    private string $author = '';

    #[ORM\Column(name: 'email', type: 'string', length: 64, nullable: false)]
    private string $email = '';

    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private ?bool $universe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWho(): ?string
    {
        return $this->who;
    }

    public function setWho(string $who): self
    {
        $this->who = $who;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getLonger(): ?int
    {
        return $this->longer;
    }

    public function setLonger(int $longer): self
    {
        $this->longer = $longer;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function isUniverse(): ?bool
    {
        return $this->universe;
    }

    public function setUniverse(bool $universe): self
    {
        $this->universe = $universe;

        return $this;
    }


}
