<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersToAcs
 */
#[ORM\Table(name: 'users_to_acs')]
#[ORM\Index(name: 'acsID', columns: ['acsID'])]
#[ORM\Index(name: 'userID', columns: ['userID'])]
#[ORM\Entity]
class UsersToAcs
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $userid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'acsID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private $acsid;


}
