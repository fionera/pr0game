<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FleetEvent
 */
#[ORM\Table(name: 'fleet_event')]
#[ORM\Index(name: 'lock', columns: ['lock', 'time'])]
#[ORM\Entity]
class FleetEvent
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'fleetID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $fleetid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'time', type: 'integer', nullable: false)]
    private $time;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'lock', type: 'string', length: 32, nullable: true)]
    private $lock;


}
