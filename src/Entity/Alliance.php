<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alliance
 */
#[ORM\Table(name: 'alliance')]
#[ORM\Index(name: 'ally_name', columns: ['ally_name'])]
#[ORM\Index(name: 'ally_tag', columns: ['ally_tag'])]
#[ORM\Index(name: 'ally_universe', columns: ['ally_universe'])]
#[ORM\Entity]
class Alliance
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_name', type: 'string', length: 50, nullable: true)]
    private ?string $allyName = '';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_tag', type: 'string', length: 20, nullable: true)]
    private ?string $allyTag = '';

    #[ORM\Column(name: 'ally_owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $allyOwner = '0';

    #[ORM\Column(name: 'ally_register_time', type: 'integer', nullable: false)]
    private string $allyRegisterTime = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_description', type: 'text', length: 65535, nullable: true)]
    private $allyDescription;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_web', type: 'string', length: 255, nullable: true)]
    private string $allyWeb = '';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_text', type: 'text', length: 65535, nullable: true)]
    private $allyText;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_image', type: 'string', length: 255, nullable: true)]
    private string $allyImage = '';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_request', type: 'string', length: 1000, nullable: true)]
    private $allyRequest;

    #[ORM\Column(name: 'ally_request_notallow', type: 'boolean', nullable: false)]
    private string $allyRequestNotallow = '0';

    #[ORM\Column(name: 'ally_request_min_points', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $allyRequestMinPoints = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ally_owner_range', type: 'string', length: 32, nullable: true)]
    private string $allyOwnerRange = '';

    #[ORM\Column(name: 'ally_members', type: 'boolean', nullable: false)]
    private string $allyMembers = '0';

    #[ORM\Column(name: 'ally_stats', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $allyStats = true;

    #[ORM\Column(name: 'ally_diplo', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $allyDiplo = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'ally_universe', type: 'boolean', nullable: false)]
    private $allyUniverse;

    #[ORM\Column(name: 'ally_max_members', type: 'integer', nullable: false, options: ['default' => 20, 'unsigned' => true])]
    private int $allyMaxMembers = 20;

    #[ORM\Column(name: 'ally_events', type: 'string', length: 55, nullable: false)]
    private string $allyEvents = '';


}
