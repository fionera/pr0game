<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Planets
 */
#[ORM\Table(name: 'planets')]
#[ORM\Index(name: 'destruyed', columns: ['destruyed'])]
#[ORM\Index(name: 'id_luna', columns: ['id_luna'])]
#[ORM\Index(name: 'id_owner', columns: ['id_owner'])]
#[ORM\Index(name: 'universe', columns: ['universe', 'galaxy', 'system', 'planet', 'planet_type'])]
#[ORM\Entity]
class Planet
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'name', type: 'string', length: 20, nullable: true, options: ['default' => 'Hauptplanet'])]
    private string $name = 'Hauptplanet';

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'id_owner', type: 'integer', nullable: true, options: ['unsigned' => true])]
    private $idOwner;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private $universe;

    #[ORM\Column(name: 'galaxy', type: 'boolean', nullable: false)]
    private string $galaxy = '0';

    #[ORM\Column(name: 'system', type: 'smallint', nullable: false)]
    private string $system = '0';

    #[ORM\Column(name: 'planet', type: 'boolean', nullable: false)]
    private string $planet = '0';

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'last_update', type: 'integer', nullable: true)]
    private $lastUpdate;

    #[ORM\Column(name: 'planet_type', type: 'string', length: 0, nullable: false, options: ['default' => 1])]
    private string $planetType = '1';

    /**
     * @var int Unixtimestamp of destruction
     */
    #[ORM\Column(name: 'destruyed', type: 'integer', nullable: false)]
    private int $destroyed = 0;

    #[ORM\Column(name: 'b_building', type: 'integer', nullable: false)]
    private string $bBuilding = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'b_building_id', type: 'text', length: 65535, nullable: true)]
    private $buildingQueue;

    #[ORM\Column(name: 'b_hangar', type: 'integer', nullable: false)]
    private string $bHangar = '0';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'b_hangar_id', type: 'text', length: 65535, nullable: true)]
    private $bHangarId;

    #[ORM\Column(name: 'b_hangar_plus', type: 'integer', nullable: false)]
    private string $bHangarPlus = '0';

    #[ORM\Column(name: 'image', type: 'string', length: 32, nullable: false, options: ['default' => 'normaltempplanet01'])]
    private string $image = 'normaltempplanet01';

    #[ORM\Column(name: 'diameter', type: 'integer', nullable: false, options: ['default' => 12800, 'unsigned' => true])]
    private int $diameter = 12800;

    #[ORM\Column(name: 'field_current', type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private string $fieldCurrent = '0';

    #[ORM\Column(name: 'field_max', type: 'smallint', nullable: false, options: ['default' => 163, 'unsigned' => true])]
    private string $fieldMax = '163';

    #[ORM\Column(name: 'temp_min', type: 'integer', nullable: false, options: ['default' => -17])]
    private int $tempMin = -17;

    #[ORM\Column(name: 'temp_max', type: 'integer', nullable: false, options: ['default' => 23])]
    private int $tempMax = 23;

    #[ORM\Column(name: 'eco_hash', type: 'string', length: 32, nullable: false)]
    private string $ecoHash = '';

    #[ORM\Column(name: 'metal', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $metal = 0.000000;

    #[ORM\Column(name: 'metal_perhour', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $metalPerhour = 0.000000;

    /**
     * @var float|null
     */
    #[ORM\Column(name: 'metal_max', type: 'float', precision: 50, scale: 0, nullable: true, options: ['default' => 100000])]
    private int $metalMax = 100000;

    #[ORM\Column(name: 'crystal', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $crystal = 0.000000;

    #[ORM\Column(name: 'crystal_perhour', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $crystalPerhour = 0.000000;

    /**
     * @var float|null
     */
    #[ORM\Column(name: 'crystal_max', type: 'float', precision: 50, scale: 0, nullable: true, options: ['default' => 100000])]
    private int $crystalMax = 100000;

    #[ORM\Column(name: 'deuterium', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $deuterium = 0.000000;

    #[ORM\Column(name: 'deuterium_perhour', type: 'float', precision: 50, scale: 6, nullable: false, options: ['default' => '0.000000'])]
    private float $deuteriumPerhour = 0.000000;

    /**
     * @var float|null
     */
    #[ORM\Column(name: 'deuterium_max', type: 'float', precision: 50, scale: 0, nullable: true, options: ['default' => 100000])]
    private int $deuteriumMax = 100000;

    #[ORM\Column(name: 'energy_used', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $energyUsed = '0';

    #[ORM\Column(name: 'energy', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $energy = '0';

    #[ORM\Column(name: 'metal_mine', type: 'integer', nullable: false)]
    private string $metalMine = '0';

    #[ORM\Column(name: 'crystal_mine', type: 'integer', nullable: false)]
    private string $crystalMine = '0';

    #[ORM\Column(name: 'deuterium_sintetizer', type: 'integer', nullable: false)]
    private string $deuteriumSintetizer = '0';

    #[ORM\Column(name: 'solar_plant', type: 'integer', nullable: false)]
    private string $solarPlant = '0';

    #[ORM\Column(name: 'fusion_plant', type: 'integer', nullable: false)]
    private string $fusionPlant = '0';

    #[ORM\Column(name: 'robot_factory', type: 'integer', nullable: false)]
    private string $robotFactory = '0';

    #[ORM\Column(name: 'nano_factory', type: 'integer', nullable: false)]
    private string $nanoFactory = '0';

    #[ORM\Column(name: 'hangar', type: 'integer', nullable: false)]
    private string $hangar = '0';

    #[ORM\Column(name: 'metal_store', type: 'integer', nullable: false)]
    private string $metalStore = '0';

    #[ORM\Column(name: 'crystal_store', type: 'integer', nullable: false)]
    private string $crystalStore = '0';

    #[ORM\Column(name: 'deuterium_store', type: 'integer', nullable: false)]
    private string $deuteriumStore = '0';

    #[ORM\Column(name: 'laboratory', type: 'integer', nullable: false)]
    private string $laboratory = '0';

    #[ORM\Column(name: 'terraformer', type: 'integer', nullable: false)]
    private string $terraformer = '0';

    #[ORM\Column(name: 'university', type: 'integer', nullable: false)]
    private string $university = '0';

    #[ORM\Column(name: 'ally_deposit', type: 'integer', nullable: false)]
    private string $allyDeposit = '0';

    #[ORM\Column(name: 'silo', type: 'integer', nullable: false)]
    private string $silo = '0';

    #[ORM\Column(name: 'mondbasis', type: 'integer', nullable: false)]
    private string $mondbasis = '0';

    #[ORM\Column(name: 'phalanx', type: 'integer', nullable: false)]
    private string $phalanx = '0';

    #[ORM\Column(name: 'sprungtor', type: 'integer', nullable: false)]
    private string $sprungtor = '0';

    #[ORM\Column(name: 'small_ship_cargo', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $smallShipCargo = '0';

    #[ORM\Column(name: 'big_ship_cargo', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $bigShipCargo = '0';

    #[ORM\Column(name: 'light_hunter', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $lightHunter = '0';

    #[ORM\Column(name: 'heavy_hunter', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $heavyHunter = '0';

    #[ORM\Column(name: 'crusher', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $crusher = '0';

    #[ORM\Column(name: 'battle_ship', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $battleShip = '0';

    #[ORM\Column(name: 'colonizer', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $colonizer = '0';

    #[ORM\Column(name: 'recycler', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $recycler = '0';

    #[ORM\Column(name: 'spy_sonde', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $spySonde = '0';

    #[ORM\Column(name: 'bomber_ship', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $bomberShip = '0';

    #[ORM\Column(name: 'solar_satelit', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $solarSatelit = '0';

    #[ORM\Column(name: 'destructor', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $destructor = '0';

    #[ORM\Column(name: 'dearth_star', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $dearthStar = '0';

    #[ORM\Column(name: 'battleship', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $battleship = '0';

    #[ORM\Column(name: 'lune_noir', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $luneNoir = '0';

    #[ORM\Column(name: 'ev_transporter', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $evTransporter = '0';

    #[ORM\Column(name: 'star_crasher', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $starCrasher = '0';

    #[ORM\Column(name: 'giga_recykler', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $gigaRecykler = '0';

    #[ORM\Column(name: 'orbital_station', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $orbitalStation = '0';

    #[ORM\Column(name: 'misil_launcher', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $misilLauncher = '0';

    #[ORM\Column(name: 'small_laser', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $smallLaser = '0';

    #[ORM\Column(name: 'big_laser', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $bigLaser = '0';

    #[ORM\Column(name: 'gauss_canyon', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $gaussCanyon = '0';

    #[ORM\Column(name: 'ionic_canyon', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $ionicCanyon = '0';

    #[ORM\Column(name: 'buster_canyon', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $busterCanyon = '0';

    #[ORM\Column(name: 'small_protection_shield', type: 'boolean', nullable: false)]
    private string $smallProtectionShield = '0';

    #[ORM\Column(name: 'planet_protector', type: 'boolean', nullable: false)]
    private string $planetProtector = '0';

    #[ORM\Column(name: 'big_protection_shield', type: 'boolean', nullable: false)]
    private string $bigProtectionShield = '0';

    #[ORM\Column(name: 'graviton_canyon', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $gravitonCanyon = '0';

    #[ORM\Column(name: 'interceptor_misil', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $interceptorMisil = '0';

    #[ORM\Column(name: 'interplanetary_misil', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $interplanetaryMisil = '0';

    #[ORM\Column(name: 'metal_mine_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $metalMinePorcent = '10';

    #[ORM\Column(name: 'crystal_mine_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $crystalMinePorcent = '10';

    #[ORM\Column(name: 'deuterium_sintetizer_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $deuteriumSintetizerPorcent = '10';

    #[ORM\Column(name: 'solar_plant_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $solarPlantPorcent = '10';

    #[ORM\Column(name: 'fusion_plant_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $fusionPlantPorcent = '10';

    #[ORM\Column(name: 'solar_satelit_porcent', type: 'string', length: 0, nullable: false, options: ['default' => 10])]
    private string $solarSatelitPorcent = '10';

    #[ORM\Column(name: 'last_jump_time', type: 'integer', nullable: false)]
    private string $lastJumpTime = '0';

    #[ORM\Column(name: 'der_metal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $derMetal = '0';

    #[ORM\Column(name: 'der_crystal', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $derCrystal = '0';

    #[ORM\Column(name: 'tf_active', type: 'boolean', nullable: false)]
    private string $tfActive = '0';

    #[ORM\OneToOne(targetEntity: Planet::class)]
    #[ORM\JoinColumn(name: 'id_luna', referencedColumnName: 'id', nullable: false)]
    private Planet|null $moon = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdOwner(): ?int
    {
        return $this->idOwner;
    }

    public function setIdOwner(?int $idOwner): self
    {
        $this->idOwner = $idOwner;

        return $this;
    }

    public function isUniverse(): ?bool
    {
        return $this->universe;
    }

    public function setUniverse(bool $universe): self
    {
        $this->universe = $universe;

        return $this;
    }

    public function isGalaxy(): ?bool
    {
        return $this->galaxy;
    }

    public function setGalaxy(bool $galaxy): self
    {
        $this->galaxy = $galaxy;

        return $this;
    }

    public function getSystem(): ?int
    {
        return $this->system;
    }

    public function setSystem(int $system): self
    {
        $this->system = $system;

        return $this;
    }

    public function isPlanet(): ?bool
    {
        return $this->planet;
    }

    public function setPlanet(bool $planet): self
    {
        $this->planet = $planet;

        return $this;
    }

    public function getLastUpdate(): ?int
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(?int $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    public function getPlanetType(): ?string
    {
        return $this->planetType;
    }

    public function setPlanetType(string $planetType): self
    {
        $this->planetType = $planetType;

        return $this;
    }

    public function getDestroyed(): ?int
    {
        return $this->destroyed;
    }

    public function setDestroyed(int $destroyed): self
    {
        $this->destroyed = $destroyed;

        return $this;
    }

    public function getBuildingTime(): ?int
    {
        return $this->bBuilding;
    }

    public function setBBuilding(int $bBuilding): self
    {
        $this->bBuilding = $bBuilding;

        return $this;
    }

    public function getBuildingQueue(): ?string
    {
        return $this->buildingQueue;
    }

    public function setBuildingQueue(?string $buildingQueue): self
    {
        $this->buildingQueue = $buildingQueue;

        return $this;
    }

    public function getBHangar(): ?int
    {
        return $this->bHangar;
    }

    public function setBHangar(int $bHangar): self
    {
        $this->bHangar = $bHangar;

        return $this;
    }

    public function getBHangarId(): ?string
    {
        return $this->bHangarId;
    }

    public function setBHangarId(?string $bHangarId): self
    {
        $this->bHangarId = $bHangarId;

        return $this;
    }

    public function getBHangarPlus(): ?int
    {
        return $this->bHangarPlus;
    }

    public function setBHangarPlus(int $bHangarPlus): self
    {
        $this->bHangarPlus = $bHangarPlus;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDiameter(): ?int
    {
        return $this->diameter;
    }

    public function setDiameter(int $diameter): self
    {
        $this->diameter = $diameter;

        return $this;
    }

    public function getFieldCurrent(): ?int
    {
        return $this->fieldCurrent;
    }

    public function setFieldCurrent(int $fieldCurrent): self
    {
        $this->fieldCurrent = $fieldCurrent;

        return $this;
    }

    public function getFieldMax(): ?int
    {
        return $this->fieldMax;
    }

    public function setFieldMax(int $fieldMax): self
    {
        $this->fieldMax = $fieldMax;

        return $this;
    }

    public function getTempMin(): ?int
    {
        return $this->tempMin;
    }

    public function setTempMin(int $tempMin): self
    {
        $this->tempMin = $tempMin;

        return $this;
    }

    public function getTempMax(): ?int
    {
        return $this->tempMax;
    }

    public function setTempMax(int $tempMax): self
    {
        $this->tempMax = $tempMax;

        return $this;
    }

    public function getEcoHash(): ?string
    {
        return $this->ecoHash;
    }

    public function setEcoHash(string $ecoHash): self
    {
        $this->ecoHash = $ecoHash;

        return $this;
    }

    public function getMetal(): ?float
    {
        return $this->metal;
    }

    public function setMetal(float $metal): self
    {
        $this->metal = $metal;

        return $this;
    }

    public function getMetalPerhour(): ?float
    {
        return $this->metalPerhour;
    }

    public function setMetalPerhour(float $metalPerhour): self
    {
        $this->metalPerhour = $metalPerhour;

        return $this;
    }

    public function getMetalMax(): ?float
    {
        return $this->metalMax;
    }

    public function setMetalMax(?float $metalMax): self
    {
        $this->metalMax = $metalMax;

        return $this;
    }

    public function getCrystal(): ?float
    {
        return $this->crystal;
    }

    public function setCrystal(float $crystal): self
    {
        $this->crystal = $crystal;

        return $this;
    }

    public function getCrystalPerhour(): ?float
    {
        return $this->crystalPerhour;
    }

    public function setCrystalPerhour(float $crystalPerhour): self
    {
        $this->crystalPerhour = $crystalPerhour;

        return $this;
    }

    public function getCrystalMax(): ?float
    {
        return $this->crystalMax;
    }

    public function setCrystalMax(?float $crystalMax): self
    {
        $this->crystalMax = $crystalMax;

        return $this;
    }

    public function getDeuterium(): ?float
    {
        return $this->deuterium;
    }

    public function setDeuterium(float $deuterium): self
    {
        $this->deuterium = $deuterium;

        return $this;
    }

    public function getDeuteriumPerhour(): ?float
    {
        return $this->deuteriumPerhour;
    }

    public function setDeuteriumPerhour(float $deuteriumPerhour): self
    {
        $this->deuteriumPerhour = $deuteriumPerhour;

        return $this;
    }

    public function getDeuteriumMax(): ?float
    {
        return $this->deuteriumMax;
    }

    public function setDeuteriumMax(?float $deuteriumMax): self
    {
        $this->deuteriumMax = $deuteriumMax;

        return $this;
    }

    public function getEnergyUsed(): ?float
    {
        return $this->energyUsed;
    }

    public function setEnergyUsed(float $energyUsed): self
    {
        $this->energyUsed = $energyUsed;

        return $this;
    }

    public function getEnergy(): ?float
    {
        return $this->energy;
    }

    public function setEnergy(float $energy): self
    {
        $this->energy = $energy;

        return $this;
    }


    public function getStarCrasher(): ?string
    {
        return $this->starCrasher;
    }

    public function setStarCrasher(string $starCrasher): self
    {
        $this->starCrasher = $starCrasher;

        return $this;
    }

    public function getGigaRecykler(): ?string
    {
        return $this->gigaRecykler;
    }

    public function setGigaRecykler(string $gigaRecykler): self
    {
        $this->gigaRecykler = $gigaRecykler;

        return $this;
    }

    public function getOrbitalStation(): ?string
    {
        return $this->orbitalStation;
    }

    public function setOrbitalStation(string $orbitalStation): self
    {
        $this->orbitalStation = $orbitalStation;

        return $this;
    }

    public function getMisilLauncher(): ?string
    {
        return $this->misilLauncher;
    }

    public function setMisilLauncher(string $misilLauncher): self
    {
        $this->misilLauncher = $misilLauncher;

        return $this;
    }

    public function getSmallLaser(): ?string
    {
        return $this->smallLaser;
    }

    public function setSmallLaser(string $smallLaser): self
    {
        $this->smallLaser = $smallLaser;

        return $this;
    }

    public function getBigLaser(): ?string
    {
        return $this->bigLaser;
    }

    public function setBigLaser(string $bigLaser): self
    {
        $this->bigLaser = $bigLaser;

        return $this;
    }

    public function getGaussCanyon(): ?string
    {
        return $this->gaussCanyon;
    }

    public function setGaussCanyon(string $gaussCanyon): self
    {
        $this->gaussCanyon = $gaussCanyon;

        return $this;
    }

    public function getIonicCanyon(): ?string
    {
        return $this->ionicCanyon;
    }

    public function setIonicCanyon(string $ionicCanyon): self
    {
        $this->ionicCanyon = $ionicCanyon;

        return $this;
    }

    public function getBusterCanyon(): ?string
    {
        return $this->busterCanyon;
    }

    public function setBusterCanyon(string $busterCanyon): self
    {
        $this->busterCanyon = $busterCanyon;

        return $this;
    }

    public function isSmallProtectionShield(): ?bool
    {
        return $this->smallProtectionShield;
    }

    public function setSmallProtectionShield(bool $smallProtectionShield): self
    {
        $this->smallProtectionShield = $smallProtectionShield;

        return $this;
    }

    public function isPlanetProtector(): ?bool
    {
        return $this->planetProtector;
    }

    public function setPlanetProtector(bool $planetProtector): self
    {
        $this->planetProtector = $planetProtector;

        return $this;
    }

    public function isBigProtectionShield(): ?bool
    {
        return $this->bigProtectionShield;
    }

    public function setBigProtectionShield(bool $bigProtectionShield): self
    {
        $this->bigProtectionShield = $bigProtectionShield;

        return $this;
    }

    public function getGravitonCanyon(): ?string
    {
        return $this->gravitonCanyon;
    }

    public function setGravitonCanyon(string $gravitonCanyon): self
    {
        $this->gravitonCanyon = $gravitonCanyon;

        return $this;
    }

    public function getInterceptorMisil(): ?string
    {
        return $this->interceptorMisil;
    }

    public function setInterceptorMisil(string $interceptorMisil): self
    {
        $this->interceptorMisil = $interceptorMisil;

        return $this;
    }

    public function getInterplanetaryMisil(): ?string
    {
        return $this->interplanetaryMisil;
    }

    public function setInterplanetaryMisil(string $interplanetaryMisil): self
    {
        $this->interplanetaryMisil = $interplanetaryMisil;

        return $this;
    }

    public function getMetalMinePorcent(): ?string
    {
        return $this->metalMinePorcent;
    }

    public function setMetalMinePorcent(string $metalMinePorcent): self
    {
        $this->metalMinePorcent = $metalMinePorcent;

        return $this;
    }

    public function getCrystalMinePorcent(): ?string
    {
        return $this->crystalMinePorcent;
    }

    public function setCrystalMinePorcent(string $crystalMinePorcent): self
    {
        $this->crystalMinePorcent = $crystalMinePorcent;

        return $this;
    }

    public function getDeuteriumSintetizerPorcent(): ?string
    {
        return $this->deuteriumSintetizerPorcent;
    }

    public function setDeuteriumSintetizerPorcent(string $deuteriumSintetizerPorcent): self
    {
        $this->deuteriumSintetizerPorcent = $deuteriumSintetizerPorcent;

        return $this;
    }

    public function getSolarPlantPorcent(): ?string
    {
        return $this->solarPlantPorcent;
    }

    public function setSolarPlantPorcent(string $solarPlantPorcent): self
    {
        $this->solarPlantPorcent = $solarPlantPorcent;

        return $this;
    }

    public function getFusionPlantPorcent(): ?string
    {
        return $this->fusionPlantPorcent;
    }

    public function setFusionPlantPorcent(string $fusionPlantPorcent): self
    {
        $this->fusionPlantPorcent = $fusionPlantPorcent;

        return $this;
    }

    public function getSolarSatelitPorcent(): ?string
    {
        return $this->solarSatelitPorcent;
    }

    public function setSolarSatelitPorcent(string $solarSatelitPorcent): self
    {
        $this->solarSatelitPorcent = $solarSatelitPorcent;

        return $this;
    }

    public function getLastJumpTime(): ?int
    {
        return $this->lastJumpTime;
    }

    public function setLastJumpTime(int $lastJumpTime): self
    {
        $this->lastJumpTime = $lastJumpTime;

        return $this;
    }

    public function getDerMetal(): ?float
    {
        return $this->derMetal;
    }

    public function setDerMetal(float $derMetal): self
    {
        $this->derMetal = $derMetal;

        return $this;
    }

    public function getDerCrystal(): ?float
    {
        return $this->derCrystal;
    }

    public function setDerCrystal(float $derCrystal): self
    {
        $this->derCrystal = $derCrystal;

        return $this;
    }

    public function isTfActive(): ?bool
    {
        return $this->tfActive;
    }

    public function setTfActive(bool $tfActive): self
    {
        $this->tfActive = $tfActive;

        return $this;
    }

    public function getMoon(): ?self
    {
        if($this->moon?->id !== 0){
            return $this->moon;
        }

        return null;
    }

    public function setMoon(self $moon): self
    {
        $this->moon = $moon;

        return $this;
    }

    public function getBBuilding(): ?int
    {
        return $this->bBuilding;
    }

    public function getMetalMine(): ?int
    {
        return $this->metalMine;
    }

    public function getCrystalMine(): ?int
    {
        return $this->crystalMine;
    }

    public function getDeuteriumSintetizer(): ?int
    {
        return $this->deuteriumSintetizer;
    }

    public function getSolarPlant(): ?int
    {
        return $this->solarPlant;
    }

    public function getFusionPlant(): ?int
    {
        return $this->fusionPlant;
    }

    public function getRobotFactory(): ?int
    {
        return $this->robotFactory;
    }

    public function getNanoFactory(): ?int
    {
        return $this->nanoFactory;
    }

    public function getHangar(): ?int
    {
        return $this->hangar;
    }

    public function getMetalStore(): ?int
    {
        return $this->metalStore;
    }

    public function getCrystalStore(): ?int
    {
        return $this->crystalStore;
    }

    public function getDeuteriumStore(): ?int
    {
        return $this->deuteriumStore;
    }

    public function getLaboratory(): ?int
    {
        return $this->laboratory;
    }

    public function getTerraformer(): ?int
    {
        return $this->terraformer;
    }

    public function getUniversity(): ?int
    {
        return $this->university;
    }

    public function getAllyDeposit(): ?int
    {
        return $this->allyDeposit;
    }

    public function getSilo(): ?int
    {
        return $this->silo;
    }

    public function getMondbasis(): ?int
    {
        return $this->mondbasis;
    }

    public function getPhalanx(): ?int
    {
        return $this->phalanx;
    }

    public function getSprungtor(): ?int
    {
        return $this->sprungtor;
    }

    public function setMetalMine(int $metalMine): self
    {
        $this->metalMine = $metalMine;

        return $this;
    }

    public function setCrystalMine(int $crystalMine): self
    {
        $this->crystalMine = $crystalMine;

        return $this;
    }

    public function setDeuteriumSintetizer(int $deuteriumSintetizer): self
    {
        $this->deuteriumSintetizer = $deuteriumSintetizer;

        return $this;
    }

    public function setSolarPlant(int $solarPlant): self
    {
        $this->solarPlant = $solarPlant;

        return $this;
    }

    public function setFusionPlant(int $fusionPlant): self
    {
        $this->fusionPlant = $fusionPlant;

        return $this;
    }

    public function setRobotFactory(int $robotFactory): self
    {
        $this->robotFactory = $robotFactory;

        return $this;
    }

    public function setNanoFactory(int $nanoFactory): self
    {
        $this->nanoFactory = $nanoFactory;

        return $this;
    }

    public function setHangar(int $hangar): self
    {
        $this->hangar = $hangar;

        return $this;
    }

    public function setMetalStore(int $metalStore): self
    {
        $this->metalStore = $metalStore;

        return $this;
    }

    public function setCrystalStore(int $crystalStore): self
    {
        $this->crystalStore = $crystalStore;

        return $this;
    }

    public function setDeuteriumStore(int $deuteriumStore): self
    {
        $this->deuteriumStore = $deuteriumStore;

        return $this;
    }

    public function setLaboratory(int $laboratory): self
    {
        $this->laboratory = $laboratory;

        return $this;
    }

    public function setTerraformer(int $terraformer): self
    {
        $this->terraformer = $terraformer;

        return $this;
    }

    public function setUniversity(int $university): self
    {
        $this->university = $university;

        return $this;
    }

    public function setAllyDeposit(int $allyDeposit): self
    {
        $this->allyDeposit = $allyDeposit;

        return $this;
    }

    public function setSilo(int $silo): self
    {
        $this->silo = $silo;

        return $this;
    }

    public function setMondbasis(int $mondbasis): self
    {
        $this->mondbasis = $mondbasis;

        return $this;
    }

    public function setPhalanx(int $phalanx): self
    {
        $this->phalanx = $phalanx;

        return $this;
    }

    public function setSprungtor(int $sprungtor): self
    {
        $this->sprungtor = $sprungtor;

        return $this;
    }

    public function getSmallShipCargo(): ?string
    {
        return $this->smallShipCargo;
    }

    public function setSmallShipCargo(string $smallShipCargo): self
    {
        $this->smallShipCargo = $smallShipCargo;

        return $this;
    }

    public function getBigShipCargo(): ?string
    {
        return $this->bigShipCargo;
    }

    public function setBigShipCargo(string $bigShipCargo): self
    {
        $this->bigShipCargo = $bigShipCargo;

        return $this;
    }

    public function getLightHunter(): ?string
    {
        return $this->lightHunter;
    }

    public function setLightHunter(string $lightHunter): self
    {
        $this->lightHunter = $lightHunter;

        return $this;
    }

    public function getHeavyHunter(): ?string
    {
        return $this->heavyHunter;
    }

    public function setHeavyHunter(string $heavyHunter): self
    {
        $this->heavyHunter = $heavyHunter;

        return $this;
    }

    public function getCrusher(): ?string
    {
        return $this->crusher;
    }

    public function setCrusher(string $crusher): self
    {
        $this->crusher = $crusher;

        return $this;
    }

    public function getBattleShip(): ?string
    {
        return $this->battleShip;
    }

    public function setBattleShip(string $battleShip): self
    {
        $this->battleShip = $battleShip;

        return $this;
    }

    public function getColonizer(): ?string
    {
        return $this->colonizer;
    }

    public function setColonizer(string $colonizer): self
    {
        $this->colonizer = $colonizer;

        return $this;
    }

    public function getRecycler(): ?string
    {
        return $this->recycler;
    }

    public function setRecycler(string $recycler): self
    {
        $this->recycler = $recycler;

        return $this;
    }

    public function getSpySonde(): ?string
    {
        return $this->spySonde;
    }

    public function setSpySonde(string $spySonde): self
    {
        $this->spySonde = $spySonde;

        return $this;
    }

    public function getBomberShip(): ?string
    {
        return $this->bomberShip;
    }

    public function setBomberShip(string $bomberShip): self
    {
        $this->bomberShip = $bomberShip;

        return $this;
    }

    public function getSolarSatelit(): ?string
    {
        return $this->solarSatelit;
    }

    public function setSolarSatelit(string $solarSatelit): self
    {
        $this->solarSatelit = $solarSatelit;

        return $this;
    }

    public function getDestructor(): ?string
    {
        return $this->destructor;
    }

    public function setDestructor(string $destructor): self
    {
        $this->destructor = $destructor;

        return $this;
    }

    public function getDearthStar(): ?string
    {
        return $this->dearthStar;
    }

    public function setDearthStar(string $dearthStar): self
    {
        $this->dearthStar = $dearthStar;

        return $this;
    }

    public function getLuneNoir(): ?string
    {
        return $this->luneNoir;
    }

    public function setLuneNoir(string $luneNoir): self
    {
        $this->luneNoir = $luneNoir;

        return $this;
    }

    public function getEvTransporter(): ?string
    {
        return $this->evTransporter;
    }

    public function setEvTransporter(string $evTransporter): self
    {
        $this->evTransporter = $evTransporter;

        return $this;
    }


}
