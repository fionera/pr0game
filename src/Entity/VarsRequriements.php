<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VarsRequriements
 */
#[ORM\Table(name: 'vars_requriements')]
#[ORM\Index(name: 'elementID', columns: ['elementID'])]
#[ORM\Index(name: 'requireID', columns: ['requireID'])]
#[ORM\Entity]
class VarsRequriements
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'elementID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $elementid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'requireID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $requireid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'requireLevel', type: 'integer', nullable: false)]
    private $requirelevel;


}
