<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Multi
 */
#[ORM\Table(name: 'multi')]
#[ORM\Index(name: 'userID', columns: ['userID'])]
#[ORM\Entity]
class Multi
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'multiID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $multiid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'userID', type: 'integer', nullable: false)]
    private $userid;


}
