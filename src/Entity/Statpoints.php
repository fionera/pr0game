<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Statpoints
 */
#[ORM\Table(name: 'statpoints')]
#[ORM\Index(name: 'id_owner', columns: ['id_owner'])]
#[ORM\Index(name: 'stat_type', columns: ['stat_type'])]
#[ORM\Index(name: 'universe', columns: ['universe'])]
#[ORM\UniqueConstraint(name: 'unique_values', columns: ['id_owner', 'stat_type'])]
#[ORM\Entity]
class Statpoints
{
    #[ORM\Id]
    #[ORM\Column(name: 'id_owner', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $owner = '0';

    #[ORM\Id]
    #[ORM\Column(name: 'id_ally', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $ally = '0';

    #[ORM\Column(name: 'stat_type', type: 'boolean', nullable: false)]
    private string $type = '0';

    #[ORM\Column(name: 'universe', type: 'boolean', nullable: false)]
    private string $universe = '0';

    #[ORM\Column(name: 'tech_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $techRank = '0';

    #[ORM\Column(name: 'tech_old_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $techOldRank = '0';

    #[ORM\Column(name: 'tech_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $techPoints = '0';

    #[ORM\Column(name: 'tech_count', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $techCount = '0';

    #[ORM\Column(name: 'build_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $buildRank = '0';

    #[ORM\Column(name: 'build_old_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $buildOldRank = '0';

    #[ORM\Column(name: 'build_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $buildPoints = '0';

    #[ORM\Column(name: 'build_count', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $buildCount = '0';

    #[ORM\Column(name: 'defs_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $defsRank = '0';

    #[ORM\Column(name: 'defs_old_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $defsOldRank = '0';

    #[ORM\Column(name: 'defs_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $defsPoints = '0';

    #[ORM\Column(name: 'defs_count', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $defsCount = '0';

    #[ORM\Column(name: 'fleet_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $fleetRank = '0';

    #[ORM\Column(name: 'fleet_old_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $fleetOldRank = '0';

    #[ORM\Column(name: 'fleet_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $fleetPoints = '0';

    #[ORM\Column(name: 'fleet_count', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $fleetCount = '0';

    #[ORM\Column(name: 'total_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $totalRank = '0';

    #[ORM\Column(name: 'total_old_rank', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $totalOldRank = '0';

    #[ORM\Column(name: 'total_points', type: 'float', precision: 50, scale: 0, nullable: false)]
    private string $totalPoints = '0';

    #[ORM\Column(name: 'total_count', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $totalCount = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?int
    {
        return $this->owner;
    }

    public function setOwner(int $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getAlly(): ?int
    {
        return $this->ally;
    }

    public function setAlly(int $ally): self
    {
        $this->ally = $ally;

        return $this;
    }

    public function isStatType(): ?bool
    {
        return $this->type;
    }

    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function isUniverse(): ?bool
    {
        return $this->universe;
    }

    public function setUniverse(bool $universe): self
    {
        $this->universe = $universe;

        return $this;
    }

    public function getTechRank(): ?int
    {
        return $this->techRank;
    }

    public function setTechRank(int $techRank): self
    {
        $this->techRank = $techRank;

        return $this;
    }

    public function getTechOldRank(): ?int
    {
        return $this->techOldRank;
    }

    public function setTechOldRank(int $techOldRank): self
    {
        $this->techOldRank = $techOldRank;

        return $this;
    }

    public function getTechPoints(): ?float
    {
        return $this->techPoints;
    }

    public function setTechPoints(float $techPoints): self
    {
        $this->techPoints = $techPoints;

        return $this;
    }

    public function getTechCount(): ?string
    {
        return $this->techCount;
    }

    public function setTechCount(string $techCount): self
    {
        $this->techCount = $techCount;

        return $this;
    }

    public function getBuildRank(): ?int
    {
        return $this->buildRank;
    }

    public function setBuildRank(int $buildRank): self
    {
        $this->buildRank = $buildRank;

        return $this;
    }

    public function getBuildOldRank(): ?int
    {
        return $this->buildOldRank;
    }

    public function setBuildOldRank(int $buildOldRank): self
    {
        $this->buildOldRank = $buildOldRank;

        return $this;
    }

    public function getBuildPoints(): ?float
    {
        return $this->buildPoints;
    }

    public function setBuildPoints(float $buildPoints): self
    {
        $this->buildPoints = $buildPoints;

        return $this;
    }

    public function getBuildCount(): ?string
    {
        return $this->buildCount;
    }

    public function setBuildCount(string $buildCount): self
    {
        $this->buildCount = $buildCount;

        return $this;
    }

    public function getDefsRank(): ?int
    {
        return $this->defsRank;
    }

    public function setDefsRank(int $defsRank): self
    {
        $this->defsRank = $defsRank;

        return $this;
    }

    public function getDefsOldRank(): ?int
    {
        return $this->defsOldRank;
    }

    public function setDefsOldRank(int $defsOldRank): self
    {
        $this->defsOldRank = $defsOldRank;

        return $this;
    }

    public function getDefsPoints(): ?float
    {
        return $this->defsPoints;
    }

    public function setDefsPoints(float $defsPoints): self
    {
        $this->defsPoints = $defsPoints;

        return $this;
    }

    public function getDefsCount(): ?string
    {
        return $this->defsCount;
    }

    public function setDefsCount(string $defsCount): self
    {
        $this->defsCount = $defsCount;

        return $this;
    }

    public function getFleetRank(): ?int
    {
        return $this->fleetRank;
    }

    public function setFleetRank(int $fleetRank): self
    {
        $this->fleetRank = $fleetRank;

        return $this;
    }

    public function getFleetOldRank(): ?int
    {
        return $this->fleetOldRank;
    }

    public function setFleetOldRank(int $fleetOldRank): self
    {
        $this->fleetOldRank = $fleetOldRank;

        return $this;
    }

    public function getFleetPoints(): ?float
    {
        return $this->fleetPoints;
    }

    public function setFleetPoints(float $fleetPoints): self
    {
        $this->fleetPoints = $fleetPoints;

        return $this;
    }

    public function getFleetCount(): ?string
    {
        return $this->fleetCount;
    }

    public function setFleetCount(string $fleetCount): self
    {
        $this->fleetCount = $fleetCount;

        return $this;
    }

    public function getTotalRank(): ?int
    {
        return $this->totalRank;
    }

    public function setTotalRank(int $totalRank): self
    {
        $this->totalRank = $totalRank;

        return $this;
    }

    public function getTotalOldRank(): ?int
    {
        return $this->totalOldRank;
    }

    public function setTotalOldRank(int $totalOldRank): self
    {
        $this->totalOldRank = $totalOldRank;

        return $this;
    }

    public function getTotalPoints(): ?float
    {
        return $this->totalPoints;
    }

    public function setTotalPoints(float $totalPoints): self
    {
        $this->totalPoints = $totalPoints;

        return $this;
    }

    public function getTotalCount(): ?string
    {
        return $this->totalCount;
    }

    public function setTotalCount(string $totalCount): self
    {
        $this->totalCount = $totalCount;

        return $this;
    }


}
