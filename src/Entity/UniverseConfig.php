<?php

namespace App\Entity;

use App\Enum\UniverseStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * Config
 */
#[ORM\Table(name: 'config')]
#[ORM\Entity]
class UniverseConfig
{
    final public const REQUEST_ATTRIBUTE = 'universe';
    final public const DEFAULT_UNIVERSE = 1;

    /**
     * @var int
     */
    #[ORM\Column(name: 'uni', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'VERSION', type: 'string', length: 8, nullable: false)]
    private $version;

    #[ORM\Column(name: 'sql_revision', type: 'integer', nullable: false)]
    private string $sqlRevision = '0';

    #[ORM\Column(name: 'users_amount', type: 'integer', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private int $usersAmount = 1;

    #[ORM\Column(name: 'building_speed', type: 'bigint', nullable: false, options: ['default' => 2500, 'unsigned' => true])]
    private string $buildingSpeed = '2500';

    #[ORM\Column(name: 'shipyard_speed', type: 'bigint', nullable: false, options: ['default' => 2500, 'unsigned' => true])]
    private string $shipyardSpeed = '2500';

    #[ORM\Column(name: 'research_speed', type: 'bigint', nullable: false, options: ['default' => 2500, 'unsigned' => true])]
    private string $researchSpeed = '2500';

    #[ORM\Column(name: 'fleet_speed', type: 'bigint', nullable: false, options: ['default' => 2500, 'unsigned' => true])]
    private string $fleetSpeed = '2500';

    #[ORM\Column(name: 'resource_multiplier', type: 'smallint', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private string $resourceMultiplier = '1';

    #[ORM\Column(name: 'storage_multiplier', type: 'smallint', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private string $storageMultiplier = '1';

    #[ORM\Column(name: 'message_delete_behavior', type: 'boolean', nullable: false)]
    private string $messageDeleteBehavior = '0';

    #[ORM\Column(name: 'message_delete_days', type: 'boolean', nullable: false, options: ['default' => 7])]
    private string $messageDeleteDays = '7';

    #[ORM\Column(name: 'halt_speed', type: 'smallint', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private string $haltSpeed = '1';

    #[ORM\Column(name: 'Fleet_Cdr', type: 'boolean', nullable: false, options: ['default' => 30])]
    private string $fleetCdr = '30';

    #[ORM\Column(name: 'Defs_Cdr', type: 'boolean', nullable: false)]
    private string $defsCdr = '0';

    #[ORM\Column(name: 'initial_fields', type: 'smallint', nullable: false, options: ['default' => 163, 'unsigned' => true])]
    private string $initialFields = '163';

    /**
     * @var string
     */
    #[ORM\Column(name: 'uni_name', type: 'string', length: 30, nullable: false)]
    private $name;

    /**
     * @var string
     */
    #[ORM\Column(name: 'game_name', type: 'string', length: 30, nullable: false)]
    private $gameName;

    #[ORM\Column(name: 'uni_status', type: 'integer', nullable: false, enumType: UniverseStatus::class, options: ['default' => UniverseStatus::STATUS_CLOSED])]
    private \App\Enum\UniverseStatus $universeStatus = UniverseStatus::STATUS_CLOSED;

    /**
     * @var string
     */
    #[ORM\Column(name: 'close_reason', type: 'text', length: 65535, nullable: false)]
    private $closeReason;

    #[ORM\Column(name: 'metal_basic_income', type: 'integer', nullable: false, options: ['default' => 20])]
    private int $metalBasicIncome = 20;

    #[ORM\Column(name: 'crystal_basic_income', type: 'integer', nullable: false, options: ['default' => 10])]
    private int $crystalBasicIncome = 10;

    #[ORM\Column(name: 'deuterium_basic_income', type: 'integer', nullable: false)]
    private string $deuteriumBasicIncome = '0';

    #[ORM\Column(name: 'energy_basic_income', type: 'integer', nullable: false)]
    private string $energyBasicIncome = '0';

    #[ORM\Column(name: 'LastSettedGalaxyPos', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $lastsettedgalaxypos = true;

    #[ORM\Column(name: 'LastSettedSystemPos', type: 'smallint', nullable: false, options: ['default' => 1, 'unsigned' => true])]
    private string $lastsettedsystempos = '1';

    #[ORM\Column(name: 'LastSettedPlanetPos', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $lastsettedplanetpos = true;

    #[ORM\Column(name: 'noobprotection', type: 'integer', nullable: false, options: ['default' => 1])]
    private int $noobprotection = 1;

    #[ORM\Column(name: 'noobprotectiontime', type: 'integer', nullable: false, options: ['default' => 5000])]
    private int $noobprotectiontime = 5000;

    #[ORM\Column(name: 'noobprotectionmulti', type: 'integer', nullable: false, options: ['default' => 5])]
    private int $noobprotectionmulti = 5;

    #[ORM\Column(name: 'forum_url', type: 'string', length: 128, nullable: false, options: ['default' => 'http://2moons.cc'])]
    private string $forumUrl = 'http://2moons.cc';

    #[ORM\Column(name: 'adm_attack', type: 'boolean', nullable: false)]
    private string $admAttack = '0';

    #[ORM\Column(name: 'debug', type: 'boolean', nullable: false)]
    private string $debug = '0';

    #[ORM\Column(name: 'lang', type: 'string', length: 2, nullable: false)]
    private string $lang = '';

    #[ORM\Column(name: 'stat', type: 'boolean', nullable: false)]
    private string $stat = '0';

    #[ORM\Column(name: 'stat_level', type: 'boolean', nullable: false, options: ['default' => 2])]
    private string $statLevel = '2';

    #[ORM\Column(name: 'stat_last_update', type: 'integer', nullable: false)]
    private string $statLastUpdate = '0';

    #[ORM\Column(name: 'stat_settings', type: 'integer', nullable: false, options: ['default' => 1000, 'unsigned' => true])]
    private int $statSettings = 1000;

    #[ORM\Column(name: 'stat_update_time', type: 'boolean', nullable: false, options: ['default' => 25])]
    private string $statUpdateTime = '25';

    #[ORM\Column(name: 'stat_last_db_update', type: 'integer', nullable: false)]
    private string $statLastDbUpdate = '0';

    #[ORM\Column(name: 'stats_fly_lock', type: 'integer', nullable: false)]
    private string $statsFlyLock = '0';

    #[ORM\Column(name: 'cron_lock', type: 'integer', nullable: false)]
    private string $cronLock = '0';

    public function getID(): int
    {
        return $this->id;
    }

    #[ORM\Column(name: 'OverviewNewsFrame', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $overviewnewsframe = true;

    /**
     * @var string
     */
    #[ORM\Column(name: 'OverviewNewsText', type: 'text', length: 65535, nullable: false)]
    private $overviewnewstext;

    #[ORM\Column(name: 'min_build_time', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $minBuildTime = true;

    #[ORM\Column(name: 'mail_active', type: 'boolean', nullable: false)]
    private string $mailActive = '0';

    #[ORM\Column(name: 'mail_use', type: 'boolean', nullable: false)]
    private string $mailUse = '0';

    #[ORM\Column(name: 'smtp_host', type: 'string', length: 64, nullable: false)]
    private string $smtpHost = '';

    #[ORM\Column(name: 'smtp_port', type: 'smallint', nullable: false)]
    private string $smtpPort = '0';

    #[ORM\Column(name: 'smtp_user', type: 'string', length: 64, nullable: false)]
    private string $smtpUser = '';

    #[ORM\Column(name: 'smtp_pass', type: 'string', length: 32, nullable: false)]
    private string $smtpPass = '';

    #[ORM\Column(name: 'smtp_ssl', type: 'string', length: 0, nullable: false)]
    private string $smtpSsl = '';

    #[ORM\Column(name: 'smtp_sendmail', type: 'string', length: 64, nullable: false)]
    private string $smtpSendmail = '';

    #[ORM\Column(name: 'smail_path', type: 'string', length: 30, nullable: false, options: ['default' => '/usr/sbin/sendmail'])]
    private string $smailPath = '/usr/sbin/sendmail';

    #[ORM\Column(name: 'user_valid', type: 'boolean', nullable: false)]
    private string $userValid = '0';

    #[ORM\Column(name: 'ga_active', type: 'string', length: 42, nullable: false)]
    private string $gaActive = '0';

    #[ORM\Column(name: 'ga_key', type: 'string', length: 42, nullable: false)]
    private string $gaKey = '';

    #[ORM\Column(name: 'moduls', type: 'string', length: 100, nullable: false)]
    private string $moduls = '';

    #[ORM\Column(name: 'trade_allowed_ships', type: 'string', length: 255, nullable: false, options: ['default' => '202,203,204,205,206,207,208,209,210,211,212,213,214,215'])]
    private string $tradeAllowedShips = '202,203,204,205,206,207,208,209,210,211,212,213,214,215';

    #[ORM\Column(name: 'trade_charge', type: 'string', length: 5, nullable: false, options: ['default' => 30])]
    private string $tradeCharge = '30';

    #[ORM\Column(name: 'max_galaxy', type: 'boolean', nullable: false, options: ['default' => 9])]
    private string $maxGalaxy = '9';

    #[ORM\Column(name: 'max_system', type: 'smallint', nullable: false, options: ['default' => 400, 'unsigned' => true])]
    private string $maxSystem = '400';

    #[ORM\Column(name: 'max_planets', type: 'boolean', nullable: false, options: ['default' => 15])]
    private string $maxPlanets = '15';

    #[ORM\Column(name: 'planet_factor', type: 'float', precision: 2, scale: 1, nullable: false, options: ['default' => '1.0'])]
    private float $planetFactor = 1.0;

    #[ORM\Column(name: 'max_elements_build', type: 'boolean', nullable: false, options: ['default' => 5])]
    private string $maxElementsBuild = '5';

    #[ORM\Column(name: 'max_elements_tech', type: 'boolean', nullable: false, options: ['default' => 2])]
    private string $maxElementsTech = '2';

    #[ORM\Column(name: 'max_elements_ships', type: 'boolean', nullable: false, options: ['default' => 10])]
    private string $maxElementsShips = '10';

    #[ORM\Column(name: 'min_player_planets', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $minPlayerPlanets = true;

    #[ORM\Column(name: 'planets_tech', type: 'boolean', nullable: false, options: ['default' => 11])]
    private string $planetsTech = '11';

    #[ORM\Column(name: 'planets_per_tech', type: 'float', precision: 2, scale: 1, nullable: false, options: ['default' => '0.5'])]
    private float $planetsPerTech = 0.5;

    #[ORM\Column(name: 'max_fleet_per_build', type: 'bigint', nullable: false, options: ['default' => 1_000_000, 'unsigned' => true])]
    private string $maxFleetPerBuild = '1000000';

    #[ORM\Column(name: 'deuterium_cost_galaxy', type: 'integer', nullable: false, options: ['default' => 10, 'unsigned' => true])]
    private int $deuteriumCostGalaxy = 10;

    #[ORM\Column(name: 'max_overflow', type: 'float', precision: 2, scale: 1, nullable: false, options: ['default' => '1.0'])]
    private float $maxOverflow = 1.0;

    #[ORM\Column(name: 'moon_factor', type: 'float', precision: 2, scale: 1, nullable: false, options: ['default' => '1.0'])]
    private float $moonFactor = 1.0;

    #[ORM\Column(name: 'moon_chance', type: 'boolean', nullable: false, options: ['default' => 20])]
    private string $moonChance = '20';

    #[ORM\Column(name: 'factor_university', type: 'boolean', nullable: false, options: ['default' => 8])]
    private string $factorUniversity = '8';

    #[ORM\Column(name: 'max_fleets_per_acs', type: 'boolean', nullable: false, options: ['default' => 16])]
    private string $maxFleetsPerAcs = '16';

    #[ORM\Column(name: 'max_participants_per_acs', type: 'boolean', nullable: false, options: ['default' => 5])]
    private string $maxParticipantsPerAcs = '5';

    #[ORM\Column(name: 'debris_moon', type: 'boolean', nullable: false)]
    private string $debrisMoon = '0';

    #[ORM\Column(name: 'vmode_min_time', type: 'integer', nullable: false, options: ['default' => 259200])]
    private int $vmodeMinTime = 259200;

    #[ORM\Column(name: 'gate_wait_time', type: 'integer', nullable: false, options: ['default' => 3600])]
    private int $gateWaitTime = 3600;

    #[ORM\Column(name: 'metal_start', type: 'integer', nullable: false, options: ['default' => 500, 'unsigned' => true])]
    private int $metalStart = 500;

    #[ORM\Column(name: 'crystal_start', type: 'integer', nullable: false, options: ['default' => 500, 'unsigned' => true])]
    private int $crystalStart = 500;

    #[ORM\Column(name: 'deuterium_start', type: 'integer', nullable: false, options: ['unsigned' => true])]
    private string $deuteriumStart = '0';

    #[ORM\Column(name: 'ttf_file', type: 'string', length: 128, nullable: false, options: ['default' => 'styles/resource/fonts/DroidSansMono.ttf'])]
    private string $ttfFile = 'styles/resource/fonts/DroidSansMono.ttf';

    #[ORM\Column(name: 'git_issues_link', type: 'string', length: 128, nullable: false, options: ['default' => '<git_issues_link>'])]
    private string $gitIssuesLink = '<git_issues_link>';

    #[ORM\Column(name: 'ref_active', type: 'boolean', nullable: false)]
    private string $refActive = '0';

    #[ORM\Column(name: 'ref_bonus', type: 'integer', nullable: false, options: ['default' => 1000, 'unsigned' => true])]
    private int $refBonus = 1000;

    #[ORM\Column(name: 'ref_minpoints', type: 'bigint', nullable: false, options: ['default' => 2000, 'unsigned' => true])]
    private string $refMinpoints = '2000';

    #[ORM\Column(name: 'ref_max_referals', type: 'boolean', nullable: false, options: ['default' => 5])]
    private string $refMaxReferals = '5';

    #[ORM\Column(name: 'del_oldstuff', type: 'boolean', nullable: false, options: ['default' => 30])]
    private string $delOldstuff = '30';

    #[ORM\Column(name: 'del_user_manually', type: 'boolean', nullable: false, options: ['default' => 7])]
    private string $delUserManually = '7';

    #[ORM\Column(name: 'del_user_automatic', type: 'boolean', nullable: false, options: ['default' => 90])]
    private string $delUserAutomatic = '90';

    #[ORM\Column(name: 'del_user_sendmail', type: 'boolean', nullable: false, options: ['default' => 21])]
    private string $delUserSendmail = '21';

    #[ORM\Column(name: 'sendmail_inactive', type: 'boolean', nullable: false)]
    private string $sendmailInactive = '0';

    #[ORM\Column(name: 'silo_factor', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $siloFactor = true;

    #[ORM\Column(name: 'timezone', type: 'string', length: 32, nullable: false, options: ['default' => 'Europe/London'])]
    private string $timezone = 'Europe/London';

    #[ORM\Column(name: 'dst', type: 'string', length: 0, nullable: false, options: ['default' => 2])]
    private string $dst = '2';

    #[ORM\Column(name: 'energySpeed', type: 'smallint', nullable: false, options: ['default' => 1])]
    private string $energyspeed = '1';

    /**
     * @var string
     */
    #[ORM\Column(name: 'disclamerAddress', type: 'text', length: 65535, nullable: false)]
    private $disclameraddress;

    /**
     * @var string
     */
    #[ORM\Column(name: 'disclamerPhone', type: 'text', length: 65535, nullable: false)]
    private $disclamerphone;

    /**
     * @var string
     */
    #[ORM\Column(name: 'disclamerMail', type: 'text', length: 65535, nullable: false)]
    private $disclamermail;

    /**
     * @var string
     */
    #[ORM\Column(name: 'disclamerNotice', type: 'text', length: 65535, nullable: false)]
    private $disclamernotice;

    #[ORM\Column(name: 'alliance_create_min_points', type: 'bigint', nullable: false, options: ['unsigned' => true])]
    private string $allianceCreateMinPoints = '0';

    #[ORM\Column(name: 'uni_type', type: 'boolean', nullable: false)]
    private string $uniType = '0';

    #[ORM\Column(name: 'galaxy_type', type: 'boolean', nullable: false)]
    private string $galaxyType = '0';

    #[ORM\Column(name: 'planet_creation', type: 'boolean', nullable: false)]
    private string $planetCreation = '0';

    #[ORM\Column(name: 'expo_ress_met_chance', type: 'boolean', nullable: false, options: ['default' => 50])]
    private string $expoRessMetChance = '50';

    #[ORM\Column(name: 'expo_ress_crys_chance', type: 'boolean', nullable: false, options: ['default' => 33])]
    private string $expoRessCrysChance = '33';

    #[ORM\Column(name: 'expo_ress_deut_chance', type: 'boolean', nullable: false, options: ['default' => 17])]
    private string $expoRessDeutChance = '17';

    #[ORM\Column(name: 'initial_temp', type: 'integer', nullable: false, options: ['default' => 50, 'unsigned' => true])]
    private int $initialTemp = 50;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'recaptchaPrivKey', type: 'string', length: 255, nullable: true)]
    private string $recaptchaprivkey = '';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'recaptchaPubKey', type: 'string', length: 255, nullable: true)]
    private string $recaptchapubkey = '';

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getSqlRevision(): int|string
    {
        return $this->sqlRevision;
    }

    public function getUsersAmount(): int
    {
        return $this->usersAmount;
    }

    public function getBuildingSpeed(): int|string
    {
        return $this->buildingSpeed;
    }

    public function getShipyardSpeed(): int|string
    {
        return $this->shipyardSpeed;
    }

    public function getResearchSpeed(): int|string
    {
        return $this->researchSpeed;
    }

    public function getFleetSpeed(): int|string
    {
        return $this->fleetSpeed;
    }

    public function getResourceMultiplier(): int|string
    {
        return $this->resourceMultiplier;
    }

    public function getStorageMultiplier(): int|string
    {
        return $this->storageMultiplier;
    }

    public function getMessageDeleteBehavior(): bool|string
    {
        return $this->messageDeleteBehavior;
    }

    public function getMessageDeleteDays(): bool|string
    {
        return $this->messageDeleteDays;
    }

    public function getHaltSpeed(): int|string
    {
        return $this->haltSpeed;
    }

    public function getFleetCdr(): bool|string
    {
        return $this->fleetCdr;
    }

    public function getDefsCdr(): bool|string
    {
        return $this->defsCdr;
    }

    public function getInitialFields(): int|string
    {
        return $this->initialFields;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGameName(): string
    {
        return $this->gameName;
    }

    public function getUniverseStatus(): UniverseStatus
    {
        return $this->universeStatus;
    }

    public function getCloseReason(): string
    {
        return $this->closeReason;
    }

    public function getMetalBasicIncome(): int
    {
        return $this->metalBasicIncome;
    }

    public function getCrystalBasicIncome(): int
    {
        return $this->crystalBasicIncome;
    }

    public function getDeuteriumBasicIncome(): int|string
    {
        return $this->deuteriumBasicIncome;
    }

    public function getEnergyBasicIncome(): int|string
    {
        return $this->energyBasicIncome;
    }

    public function isLastsettedgalaxypos(): bool
    {
        return $this->lastsettedgalaxypos;
    }

    public function getLastsettedsystempos(): int|string
    {
        return $this->lastsettedsystempos;
    }

    public function isLastsettedplanetpos(): bool
    {
        return $this->lastsettedplanetpos;
    }

    public function getNoobprotection(): int
    {
        return $this->noobprotection;
    }

    public function getNoobprotectiontime(): int
    {
        return $this->noobprotectiontime;
    }

    public function getNoobprotectionmulti(): int
    {
        return $this->noobprotectionmulti;
    }

    public function getForumUrl(): string
    {
        return $this->forumUrl;
    }

    public function getAdmAttack(): bool|string
    {
        return $this->admAttack;
    }

    public function getDebug(): bool|string
    {
        return $this->debug;
    }

    public function getLang(): string
    {
        return $this->lang;
    }

    public function getStat(): bool|string
    {
        return $this->stat;
    }

    public function getStatLevel(): bool|string
    {
        return $this->statLevel;
    }

    public function getStatLastUpdate(): int|string
    {
        return $this->statLastUpdate;
    }

    public function getStatSettings(): int
    {
        return $this->statSettings;
    }

    public function getStatUpdateTime(): bool|string
    {
        return $this->statUpdateTime;
    }

    public function getStatLastDbUpdate(): int|string
    {
        return $this->statLastDbUpdate;
    }

    public function getStatsFlyLock(): int|string
    {
        return $this->statsFlyLock;
    }

    public function getCronLock(): int|string
    {
        return $this->cronLock;
    }

    public function isOverviewnewsframe(): bool
    {
        return $this->overviewnewsframe;
    }

    public function getOverviewnewstext(): string
    {
        return $this->overviewnewstext;
    }

    public function isMinBuildTime(): bool
    {
        return $this->minBuildTime;
    }

    public function getMailActive(): bool|string
    {
        return $this->mailActive;
    }

    public function getMailUse(): bool|string
    {
        return $this->mailUse;
    }

    public function getSmtpHost(): string
    {
        return $this->smtpHost;
    }

    public function getSmtpPort(): int|string
    {
        return $this->smtpPort;
    }

    public function getSmtpUser(): string
    {
        return $this->smtpUser;
    }

    public function getSmtpPass(): string
    {
        return $this->smtpPass;
    }

    public function getSmtpSsl(): string
    {
        return $this->smtpSsl;
    }

    public function getSmtpSendmail(): string
    {
        return $this->smtpSendmail;
    }

    public function getSmailPath(): string
    {
        return $this->smailPath;
    }

    public function getUserValid(): bool|string
    {
        return $this->userValid;
    }

    public function getGaActive(): string
    {
        return $this->gaActive;
    }

    public function getGaKey(): string
    {
        return $this->gaKey;
    }

    public function getModuls(): string
    {
        return $this->moduls;
    }

    public function getTradeAllowedShips(): string
    {
        return $this->tradeAllowedShips;
    }

    public function getTradeCharge(): string
    {
        return $this->tradeCharge;
    }

    public function getMaxGalaxy(): bool|string
    {
        return $this->maxGalaxy;
    }

    public function getMaxSystem(): int|string
    {
        return $this->maxSystem;
    }

    public function getMaxPlanets(): bool|string
    {
        return $this->maxPlanets;
    }

    public function getPlanetFactor(): float
    {
        return $this->planetFactor;
    }

    public function getMaxElementsBuild(): bool|string
    {
        return $this->maxElementsBuild;
    }

    public function getMaxElementsTech(): bool|string
    {
        return $this->maxElementsTech;
    }

    public function getMaxElementsShips(): bool|string
    {
        return $this->maxElementsShips;
    }

    public function isMinPlayerPlanets(): bool
    {
        return $this->minPlayerPlanets;
    }

    public function getPlanetsTech(): bool|string
    {
        return $this->planetsTech;
    }

    public function getPlanetsPerTech(): float
    {
        return $this->planetsPerTech;
    }

    public function getMaxFleetPerBuild(): int|string
    {
        return $this->maxFleetPerBuild;
    }

    public function getDeuteriumCostGalaxy(): int
    {
        return $this->deuteriumCostGalaxy;
    }

    public function getMaxOverflow(): float
    {
        return $this->maxOverflow;
    }

    public function getMoonFactor(): float
    {
        return $this->moonFactor;
    }

    public function getMoonChance(): bool|string
    {
        return $this->moonChance;
    }

    public function getFactorUniversity(): bool|string
    {
        return $this->factorUniversity;
    }

    public function getMaxFleetsPerAcs(): bool|string
    {
        return $this->maxFleetsPerAcs;
    }

    public function getMaxParticipantsPerAcs(): bool|string
    {
        return $this->maxParticipantsPerAcs;
    }

    public function getDebrisMoon(): bool|string
    {
        return $this->debrisMoon;
    }

    public function getVmodeMinTime(): int
    {
        return $this->vmodeMinTime;
    }

    public function getGateWaitTime(): int
    {
        return $this->gateWaitTime;
    }

    public function getMetalStart(): int
    {
        return $this->metalStart;
    }

    public function getCrystalStart(): int
    {
        return $this->crystalStart;
    }

    public function getDeuteriumStart(): int|string
    {
        return $this->deuteriumStart;
    }

    public function getTtfFile(): string
    {
        return $this->ttfFile;
    }

    public function getGitIssuesLink(): string
    {
        return $this->gitIssuesLink;
    }

    public function getRefActive(): bool|string
    {
        return $this->refActive;
    }

    public function getRefBonus(): int
    {
        return $this->refBonus;
    }

    public function getRefMinpoints(): int|string
    {
        return $this->refMinpoints;
    }

    public function getRefMaxReferals(): bool|string
    {
        return $this->refMaxReferals;
    }

    public function getDelOldstuff(): bool|string
    {
        return $this->delOldstuff;
    }

    public function getDelUserManually(): bool|string
    {
        return $this->delUserManually;
    }

    public function getDelUserAutomatic(): bool|string
    {
        return $this->delUserAutomatic;
    }

    public function getDelUserSendmail(): bool|string
    {
        return $this->delUserSendmail;
    }

    public function getSendmailInactive(): bool|string
    {
        return $this->sendmailInactive;
    }

    public function isSiloFactor(): bool
    {
        return $this->siloFactor;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function getDst(): string
    {
        return $this->dst;
    }

    public function getEnergyspeed(): int|string
    {
        return $this->energyspeed;
    }

    public function getDisclameraddress(): string
    {
        return $this->disclameraddress;
    }

    public function getDisclamerphone(): string
    {
        return $this->disclamerphone;
    }

    public function getDisclamermail(): string
    {
        return $this->disclamermail;
    }

    public function getDisclamernotice(): string
    {
        return $this->disclamernotice;
    }

    public function getAllianceCreateMinPoints(): int|string
    {
        return $this->allianceCreateMinPoints;
    }

    public function getUniType(): bool|string
    {
        return $this->uniType;
    }

    public function getGalaxyType(): bool|string
    {
        return $this->galaxyType;
    }

    public function getPlanetCreation(): bool|string
    {
        return $this->planetCreation;
    }

    public function getExpoRessMetChance(): bool|string
    {
        return $this->expoRessMetChance;
    }

    public function getExpoRessCrysChance(): bool|string
    {
        return $this->expoRessCrysChance;
    }

    public function getExpoRessDeutChance(): bool|string
    {
        return $this->expoRessDeutChance;
    }

    public function getInitialTemp(): int
    {
        return $this->initialTemp;
    }

    public function getRecaptchaprivkey(): ?string
    {
        return $this->recaptchaprivkey;
    }

    public function getRecaptchapubkey(): ?string
    {
        return $this->recaptchapubkey;
    }
}
