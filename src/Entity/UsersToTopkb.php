<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersToTopkb
 */
#[ORM\Table(name: 'users_to_topkb')]
#[ORM\Index(name: 'rid', columns: ['rid', 'role'])]
#[ORM\Entity]
class UsersToTopkb
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'rid', type: 'string', length: 32, nullable: false)]
    private $rid;

    /**
     * @var int
     */
    #[ORM\Column(name: 'uid', type: 'integer', nullable: false)]
    private $uid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'username', type: 'string', length: 128, nullable: false)]
    private $username;

    /**
     * @var int
     */
    #[ORM\Column(name: 'role', type: 'smallint', nullable: false)]
    private $role;


}
