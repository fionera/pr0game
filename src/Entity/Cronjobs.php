<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cronjobs
 */
#[ORM\Table(name: 'cronjobs')]
#[ORM\Index(name: 'isActive', columns: ['isActive', 'nextTime', 'lock', 'cronjobID'])]
#[ORM\Entity]
class Cronjobs
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'cronjobID', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $cronjobid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 32, nullable: false)]
    private $name;

    #[ORM\Column(name: 'isActive', type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $isactive = true;

    /**
     * @var string
     */
    #[ORM\Column(name: 'min', type: 'string', length: 32, nullable: false)]
    private $min;

    /**
     * @var string
     */
    #[ORM\Column(name: 'hours', type: 'string', length: 32, nullable: false)]
    private $hours;

    /**
     * @var string
     */
    #[ORM\Column(name: 'dom', type: 'string', length: 32, nullable: false)]
    private $dom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'month', type: 'string', length: 32, nullable: false)]
    private $month;

    /**
     * @var string
     */
    #[ORM\Column(name: 'dow', type: 'string', length: 32, nullable: false)]
    private $dow;

    /**
     * @var string
     */
    #[ORM\Column(name: 'class', type: 'string', length: 32, nullable: false)]
    private $class;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'nextTime', type: 'integer', nullable: true)]
    private $nexttime;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'lock', type: 'string', length: 32, nullable: true)]
    private $lock;


}
