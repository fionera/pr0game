<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketCategory
 */
#[ORM\Table(name: 'ticket_category')]
#[ORM\Entity]
class TicketCategory
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'categoryID', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $categoryid;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 32, nullable: false)]
    private $name;


}
