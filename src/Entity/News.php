<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * News
 */
#[ORM\Table(name: 'news')]
#[ORM\Entity]
class News
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    #[ORM\Column(name: 'user', type: 'string', length: 64, nullable: false)]
    private ?string $user = null;

    #[ORM\Column(name: 'date', type: 'integer', nullable: false)]
    private ?int $date = null;

    #[ORM\Column(name: 'title', type: 'string', length: 64, nullable: false)]
    private ?string $title = null;

    #[ORM\Column(name: 'text', type: 'text', length: 65535, nullable: false)]
    private ?string $text = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }


}
