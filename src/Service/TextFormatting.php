<?php

namespace App\Service;

use DateInterval;
use DateTime;

class TextFormatting
{
    public static function makeBR(string $text): string
    {
        $BR = "<br>\n";

        return strtr($text, [
            "\r\n" => $BR,
            "\r" => $BR,
            "\n" => $BR
        ]);
    }

    public static function prettyNumber($n, $dec = 0)
    {
        return number_format(self::floatToString($n, $dec), $dec, ',', '.');
    }

    public static function floatToString($number, $Pro = 0, $output = false)
    {
        if ($output) {
            return str_replace(",", ".", sprintf("%." . $Pro . "f", $number));
        }
        return sprintf("%." . $Pro . "f", $number);
    }

    public static function prettyTime($seconds)
    {
        // If we have less than 0 seconds, we force to 0.
        if ($seconds <= 0) {
            $seconds = 0;
        }

        $dt = new DateTime();
        $dt->add(new DateInterval("PT${seconds}S"));
        return $dt->diff(new DateTime())->format('%dd %Hh %Im %Ss');
    }
}