<?php

namespace App\Service;

class ColorService
{

    public function getColors() {
        if (true) {
            return [
                'colorMission2friend' => '#ff00ff',

                'colorMission1Own' => '#66cc33',
                'colorMission2Own' => '#339966',
                'colorMission3Own' => '#5bf1c2',
                'colorMission4Own' => '#cf79de',
                'colorMission5Own' => '#80a0c0',
                'colorMission6Own' => '#ffcc66',
                'colorMission7Own' => '#c1c1c1',
                'colorMission7OwnReturn' => '#cf79de',
                'colorMission8Own' => '#ceff68',
                'colorMission9Own' => '#ffff99',
                'colorMission10Own' => '#ffcc66',
                'colorMission15Own' => '#5bf1c2',
                'colorMission16Own' => '#5bf1c2',
                'colorMission17Own' => '#5bf1c2',

                'colorMissionReturnOwn' => '#6e8eea',

                'colorMission1Foreign' => '#ff0000',
                'colorMission2Foreign' => '#aa0000',
                'colorMission3Foreign' => '#00ff00',
                'colorMission4Foreign' => '#ad57bc',
                'colorMission5Foreign' => '#3399cc',
                'colorMission6Foreign' => '#ff6600',
                'colorMission7Foreign' => '#00ff00',
                'colorMission8Foreign' => '#acdd46',
                'colorMission9Foreign' => '#dddd77',
                'colorMission10Foreign' => '#ff6600',
                'colorMission15Foreign' => '#39d0a0',
                'colorMission16Foreign' => '#39d0a0',
                'colorMission17Foreign' => '#39d0a0',

                'colorMissionReturnForeign' => '#6e8eea',

                'colorStaticTimer' => '#ffff00',
            ];
        }

        return [
            'colorMission2friend' => $USER['colorMission2friend'],

            'colorMission1Own' => $USER['colorMission1Own'],
            'colorMission2Own' => $USER['colorMission2Own'],
            'colorMission3Own' => $USER['colorMission3Own'],
            'colorMission4Own' => $USER['colorMission4Own'],
            'colorMission5Own' => $USER['colorMission5Own'],
            'colorMission6Own' => $USER['colorMission6Own'],
            'colorMission7Own' => $USER['colorMission7Own'],
            'colorMission7OwnReturn' => $USER['colorMission7OwnReturn'],
            'colorMission8Own' => $USER['colorMission8Own'],
            'colorMission9Own' => $USER['colorMission9Own'],
            'colorMission10Own' => $USER['colorMission10Own'],
            'colorMission15Own' => $USER['colorMission15Own'],
            'colorMission16Own' => $USER['colorMission16Own'],
            'colorMission17Own' => $USER['colorMission17Own'],

            'colorMissionReturnOwn' => $USER['colorMissionReturnOwn'],

            'colorMission1Foreign' => $USER['colorMission1Foreign'],
            'colorMission2Foreign' => $USER['colorMission2Foreign'],
            'colorMission3Foreign' => $USER['colorMission3Foreign'],
            'colorMission4Foreign' => $USER['colorMission4Foreign'],
            'colorMission5Foreign' => $USER['colorMission5Foreign'],
            'colorMission6Foreign' => $USER['colorMission6Foreign'],
            'colorMission7Foreign' => $USER['colorMission7Foreign'],
            'colorMission8Foreign' => $USER['colorMission8Foreign'],
            'colorMission9Foreign' => $USER['colorMission9Foreign'],
            'colorMission10Foreign' => $USER['colorMission10Foreign'],
            'colorMission15Foreign' => $USER['colorMission15Foreign'],
            'colorMission16Foreign' => $USER['colorMission16Foreign'],
            'colorMission17Foreign' => $USER['colorMission17Foreign'],

            'colorMissionReturnForeign' => $USER['colorMissionReturnForeign'],

            'colorStaticTimer' => $USER['colorStaticTimer'],
        ];
    }

    public function player_signal_colors($USER = null)
    {
        if (true) {
            return [
                'colorPositive' => '#00ff00',
                'colorNegative' => '#ff0000',
                'colorNeutral' => '#ffd600',
            ];
        }

        return [
            'colorPositive' => $USER['colorPositive'],
            'colorNegative' => $USER['colorNegative'],
            'colorNeutral' => $USER['colorNeutral'],
        ];
    }

}