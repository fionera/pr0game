<?php

namespace App\Service;

use App\Entity\Topkb;
use App\Entity\UniverseConfig;
use App\Entity\User;
use App\Entity\UsersToTopkb;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

class BattlehallService
{
    private EntityManagerInterface $entityManager;
    private Connection $connection;

    public function __construct(EntityManagerInterface $entityManager, Connection $connection)
    {
        $this->entityManager = $entityManager;
        $this->connection = $connection;
    }

    public function getBattleHallList(UniverseConfig $universe): array
    {
        $query = "SELECT *,
       (select GROUP_CONCAT(`username` SEPARATOR ' & ')
        FROM (SELECT DISTINCT IF(:usersToTopKB.`username` = '', :users.`username`,
                                 :usersToTopKB.`username`) as `username`
              FROM :usersToTopKB
                       LEFT JOIN :users ON uid = :users.id
              WHERE :usersToTopKB.`rid` = :topKB.`rid`
                AND `role` = :attackerRole) attackers) as `attacker`,
       (select GROUP_CONCAT(`username` SEPARATOR ' & ')
        FROM (SELECT DISTINCT IF(:usersToTopKB.`username` = '', :users.`username`,
                                 :usersToTopKB.`username`) as `username`
              FROM :usersToTopKB
                       LEFT JOIN :users ON uid = :users.id
              WHERE :usersToTopKB.`rid` = :topKB.`rid`
                AND `role` = :defenderRole) defenders) as `defender`
FROM :topKB
WHERE `universe` = :universe
  AND `time` < :time
ORDER BY `units` DESC
LIMIT 100;";

        $tables = [
            ":usersToTopKB" => $this->entityManager->getClassMetadata(UsersToTopkb::class)->getTableName(),
            ":topKB" => $this->entityManager->getClassMetadata(Topkb::class)->getTableName(),
            ":users" => $this->entityManager->getClassMetadata(User::class)->getTableName(),
        ];

        // replace the table names in the query template
        $query = str_replace(array_keys($tables), array_values($tables), $query);

        $statement = $this->connection->prepare($query);
        $statement->bindValue('attackerRole', 1); //TODO: Add enum
        $statement->bindValue('defenderRole', 2); //TODO: Add enum
        $statement->bindValue('universe', $universe->getID());
        $statement->bindValue('time', time() - 21600);

        $result = $statement->executeQuery();

        return array_map(static fn($hallRow) => [
            'result' => $hallRow['result'],
            'time' => (new \DateTime())
                ->setTimezone(new \DateTimeZone($universe->getTimezone()))
                ->setTimestamp($hallRow['time'])
                ->format('d. M Y, H:i:s'),
            'units' => $hallRow['units'],
            'rid' => $hallRow['rid'],
            'attacker' => $hallRow['attacker'],
            'defender' => $hallRow['defender'],
        ], $result->fetchAllAssociative());
    }
}