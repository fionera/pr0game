<?php

namespace App\Service;

use App\Entity\Planet;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class BuildInfoService
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function getMoonInfo(User $user, Planet $planet)
    {
        $moon = $planet->getMoon();
        // TODO: remove magic number
        // If we are a moon, resolve the corresponding planet
        if ($moon === null && $planet->getPlanetType() === '3') {
            $moon = $this->entityManager->getRepository(Planet::class)
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.id_luna >= :id_luna')
                ->setParameter('id_luna', $planet->getId())
                ->getQuery()->getSingleScalarResult();
        }

        return [
            'id' => $planet->getId(),
            'name' => $planet->getName(),
            'image' => $planet->getImage(),
            'planet_type' => $planet->getPlanetType(),
            'build' => $this->getBuildingsInfo($user, $moon),
        ];
    }

    public function getBuildingsInfo(User $user, ?Planet $planet)
    {
        if ($planet !== null && !empty($planet->getBuildingQueue())) {
            $queue = unserialize($planet->getBuildingQueue(), ['allowed_classes' => false]);

            $timeLeft = $user->isUrlaubsModus() ?
                $planet->getBuildingTime() - $user->getUrlaubsStart() : $planet->getBuildingTime() - time();

            return [
                'id' => $queue[0][0],
                'level' => $queue[0][1],
                'timeleft' => $timeLeft,
                'time' => $planet->getBuildingTime(),
                'starttime' => TextFormatting::prettyTime($timeLeft),
            ];
        }

        return false;
    }

    private function getTechInfo(User $user)
    {
        if (!empty($user->getTechQueue())) {
            $queue = unserialize($user->getTechQueue(), ['allowed_classes' => false]);

            $timeLeft = $user->isUrlaubsModus() ?
                $user->getTechTime() - $user->getUrlaubsStart() : $user->getTechTime() - time();

            return [
                'id' => $queue[0][0],
                'level' => $queue[0][1],
                'timeleft' => $timeLeft,
                'time' => $user->getTechTime(),
                'starttime' => TextFormatting::prettyTime($timeLeft),
            ];
        }

        return false;
    }

    private function getFleetInfo(User $user, Planet $planet)
    {
        //TODO: Hangars
//        if (!empty($PLANET['b_hangar_id'])) {
//            $Queue = unserialize($PLANET['b_hangar_id']);
//            $time = BuildFunctions::getBuildingTime($USER, $PLANET, $Queue[0][0]) * $Queue[0][1];
//            $buildInfo['fleet'] = [
//                'id' => $Queue[0][0],
//                'level' => $Queue[0][1],
//                'timeleft' => $time - $PLANET['b_hangar'],
//                'time' => $time,
//                'starttime' => pretty_time($time - $PLANET['b_hangar']),
//            ];
//        }

        return false;
    }

    public function getPlanetInfo(User $user, Planet $planet)
    {
        return [
            'buildings' => $this->getBuildingsInfo($user, $planet),
            'fleet' => $this->getFleetInfo($user, $planet),
            'tech' => $this->getTechInfo($user),
        ];
    }
}