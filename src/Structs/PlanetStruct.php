<?php

namespace App\Structs;

use App\Entity\Planet;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PlanetStruct
{

    public static function getMaxPlanetFields(Planet $planet, ParameterBagInterface $params): float
    {
        return $planet->getFieldMax() +
            ($planet->getTerraformer() * $params->get('game.fields_by_terraformer')) +
            ($planet->getMondbasis() * $params->get('game.fields_by_moonbasis_level'));
    }
}