<?php

use App\Service\TextFormatting;

function getRestTimeFormat($seconds, $shorten = false)
{
    $timethresh = [31536000, 2592000, 604800, 86400, 3600, 60, 1]; // y,m,w,d,h,m,s
    $ax = [0, 0, 0, 0, 0, 0, 0];
    $fmt = ["Y", "M", "W", "d", "h", "m", "s"];
    $longest = -1;
    foreach ($timethresh as $t => $v) {
        $inside = floor($seconds / $v);
        if ($inside == 0 && $longest == $t - 1) {
            $longest = $t;
        }
        $seconds -= $inside * $v;
        $ax[$t] = $inside;
    }
    $rstr = "";
    $longest = $longest + 1;
    for ($i = $longest; $i < 7 && ($i - $longest < 3 || $shorten != true); $i++) {
        if ($i > 3) {
            $rstr = $rstr . " " . sprintf('%02d', $ax[$i]) . $fmt[$i] . " ";
        } else {
            $rstr = $rstr . " " . $ax[$i] . $fmt[$i] . " ";
        }
    }
    return trim($rstr);
}

function shortly_number($number, $decial = null)
{
    $negate = $number < 0 ? -1 : 1;
    $number = abs($number);
    $unit = ["", "K", "M", "B", "T", "Q", "Q+", "S", "S+", "O", "N"];
    $key = 0;

    if ($number >= 1000000) {
        ++$key;
        while ($number >= 1000000) {
            ++$key;
            $number = $number / 1000000;
        }
    } elseif ($number >= 1000) {
        ++$key;
        $number = $number / 1000;
    }

    if (!is_numeric($decial)) {
        $decial = ((int)(((int)$number != $number) && $key != 0 && $number != 0 && $number < 100));
    }
    return TextFormatting::prettyNumber($negate * $number, $decial) . '&nbsp;' . $unit[$key];
}

function isModuleAvailable() {
    return true;
}