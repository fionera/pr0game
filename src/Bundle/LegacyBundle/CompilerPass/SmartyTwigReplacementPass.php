<?php

namespace App\Bundle\LegacyBundle\CompilerPass;

use App\Bundle\LegacyBundle\Engine\SmartyEngine;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\TemplateNameParserInterface;

class SmartyTwigReplacementPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $container->register(TemplateNameParserInterface::class, TemplateNameParser::class);

        $container->setDefinition(SmartyEngine::class,
            new Definition(SmartyEngine::class, [
                new Reference(TemplateNameParserInterface::class),
                [
                    'template_dir' => $container->getParameter('kernel.project_dir') . '/templates/',
                    'cache_dir' => $container->getParameter('kernel.cache_dir') . '/smarty/',
                    'compile_dir' => $container->getParameter('kernel.build_dir') . '/smarty/',
                ]
            ])
        );

        $container->setAlias('twig', SmartyEngine::class);
        $container->setAlias("Twig\Environment", 'twig');
    }
}