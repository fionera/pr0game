<?php

namespace App\Bundle\LegacyBundle\CompilerPass;

use App\Bundle\LegacyBundle\Translation\LanguageLoader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Finder\Finder;

class CollectLanguagesPass implements CompilerPassInterface
{
    final public const LOADER_TAG = 'legacy';

    public function process(ContainerBuilder $container)
    {
        $container
            ->setDefinition('translation.loader.' . self::LOADER_TAG,
                (new Definition(LanguageLoader::class))
                    ->addArgument(new Reference('parameter_bag'))
                    ->addTag('translation.loader', ['alias' => self::LOADER_TAG])
            );

        $locales = [];
        $fileFinder = (new Finder())
            ->directories()
            ->depth(0)
            ->in($container->getParameter('translator.default_path'));

        /** @var \SplFileInfo $file */
        foreach ($fileFinder as $file) {
            $fileName = $file->getFilename();
            $locales[$fileName] = [
                implode('.', ["messages", $fileName, self::LOADER_TAG])
            ];
        }

        // We override the files that are known to the translator with fake files.
        // With these we can use the legacy translations without any other change.
        $translatorDef = $container->getDefinition('translator.default');
        $translatorDef->replaceArgument(4, array_replace(
            $translatorDef->getArgument(4),
            [
                'resource_files' => $locales
            ]
        ));

        $container->setParameter('kernel.enabled_locales', array_keys($locales));
    }
}