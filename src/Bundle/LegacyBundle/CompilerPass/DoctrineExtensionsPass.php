<?php

namespace App\Bundle\LegacyBundle\CompilerPass;

use App\Bundle\LegacyBundle\DoctrineExtension\TablePrefix;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class DoctrineExtensionsPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $container->setDefinition(
            'doctrine.extension.table_prefix',
            (new Definition(TablePrefix::class))
                ->addArgument('uni1_')
                ->addTag('doctrine.event_listener', [
                    'event' => Events::loadClassMetadata,
                ])
        );
    }
}