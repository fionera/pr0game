<?php

namespace App\Bundle\LegacyBundle;

use App\Bundle\LegacyBundle\CompilerPass\CollectLanguagesPass;
use App\Bundle\LegacyBundle\CompilerPass\DoctrineExtensionsPass;
use App\Bundle\LegacyBundle\CompilerPass\SmartyTwigReplacementPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LegacyBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SmartyTwigReplacementPass());
        $container->addCompilerPass(new CollectLanguagesPass());
        $container->addCompilerPass(new DoctrineExtensionsPass());
    }
}