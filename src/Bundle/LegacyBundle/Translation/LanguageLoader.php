<?php

namespace App\Bundle\LegacyBundle\Translation;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;

class LanguageLoader implements LoaderInterface
{
    private const ALLOWED_INTERSECTIONS = [
        'ma_all',
        "fcp_colony",
        "fl_simulate",
        "type_mission_1",
        "type_mission_2",
        "type_mission_3",
        "type_mission_4",
        "type_mission_5",
        "type_mission_6",
        "type_mission_7",
        "type_mission_8",
        "type_mission_9",
        "type_mission_15",
        "type_mission_16",
        "type_mission_17",
        "type_planet_short_3",
        "type_planet_1",
        "type_planet_2",
        "type_planet_3",
    ];

    public function __construct(private readonly ParameterBagInterface $params)
    {
    }

    public function load(mixed $resource, string $locale, string $domain = 'messages'): MessageCatalogue
    {
        $langPath = $this->params->get('translator.default_path') . '/' . $locale;

        $messageCatalogue = new MessageCatalogue($locale);
        $messageCatalogue->add($this->loadLanguage($langPath, $locale));
        return $messageCatalogue;
    }

    private function loadLanguage(string $path, string $locale): array
    {
        // define both LNG and languageKeys
        // we use languageKeys to verify there are no overlapping language keys
        $languageKeys = [];

        $fileFinder = new Finder();
        $fileFinder->files()->in($path);
        /** @var \SplFileInfo $file */
        foreach ($fileFinder as $file) {
            // We load the template files as normal language key.
            if ($file->getExtension() === 'txt') {
                if (!empty($languageKeys[$file->getFilename()])) {
                    throw new \RuntimeException("Template with overlapping language key found in: " . $locale . "\n" . $file->getFilename());
                }
                $languageKeys[$file->getBasename('.' . $file->getExtension())] = file_get_contents($file->getPathname());
                continue;
            }

            if ($file->getExtension() !== 'php') {
                continue;
            }

            $LNG = [];
            include $file->getPathname();

            $intersections = array_filter(array_intersect_key($languageKeys, $LNG), static fn($key) => !in_array($key, self::ALLOWED_INTERSECTIONS, true), ARRAY_FILTER_USE_KEY);

            if (count($intersections) !== 0) {
                throw new \RuntimeException("Overlapping language keys found in: " . $locale . "\n" . var_export($intersections, true));
            }

            /** @noinspection SlowArrayOperationsInLoopInspection */
            $languageKeys = array_merge($languageKeys, $LNG);
        }

        return $languageKeys;
    }
}