<?php

namespace App\Bundle\GameBundle;

use App\Bundle\LegacyBundle\CompilerPass\CollectLanguagesPass;
use App\Bundle\LegacyBundle\CompilerPass\DoctrineExtensionsPass;
use App\Bundle\LegacyBundle\CompilerPass\SmartyTwigReplacementPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GameBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
    }
}