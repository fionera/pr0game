<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Translation\LocaleSwitcher;

class LocaleSwitchSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly LocaleSwitcher $localeSwitcher)
    {
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $session = $event->getRequest()->getSession();
        if ($locale = $event->getRequest()->get('lang', false)) {
            $session->set('_locale', $locale);
        }

        $this->localeSwitcher->setLocale(
            $session->get('_locale', $event->getRequest()->getLocale())
        );
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
