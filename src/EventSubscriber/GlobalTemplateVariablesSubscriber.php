<?php

namespace App\EventSubscriber;

use App\Bundle\LegacyBundle\Engine\SmartyEngine;
use App\Entity\UniverseConfig;
use App\Enum\UniverseStatus;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

class GlobalTemplateVariablesSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ParameterBagInterface  $params,
        /**
         * We can override the annotation,
         * because there are no other providers
         */
        private readonly TranslatorInterface    $translator,
        private readonly SmartyEngine           $engine,
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    public function onKernelController(ControllerEvent $event): void
    {
        /** @var UniverseConfig $universe */
        $universe = $event->getRequest()->get(UniverseConfig::REQUEST_ATTRIBUTE);

        /** @var UniverseConfig[] $allUniverses */
        $allUniverses = $this->entityManager->getRepository(UniverseConfig::class)->findAll();
        $universeEntries = [];
        foreach ($allUniverses as $u) {
            $universeEntries[$u->getID()] = match ($u->getUniverseStatus()) {
                UniverseStatus::STATUS_CLOSED => $u->getName() . $this->translator->trans('uni_closed'),
                UniverseStatus::STATUS_REG_ONLY => $u->getName() . $this->translator->trans('uni_reg_open'),
                UniverseStatus::STATUS_LOGIN_ONLY => $u->getName() . $this->translator->trans('uni_reg_closed'),
                default => $u->getName(),
            };
        }

        $vars = [
            'gameName' => $universe->getGameName(),
            'game_name' => $universe->getGameName(),
            'uni_name' => $universe->getName(),
            'UNI' => $universe->getID(),
            'universeSelect' => $universeEntries,
            'universeSelected' => $universe->getID(),
            'isMultiUniverse' => count($universeEntries) > 1,

            'Offset' => 0, //TODO: Set Timezone offset
            'date' => explode("|", date('Y\|n\|j\|G\|i\|s\|Z')),
            'TIMEZONESTRING' => '', //TODO: Set Timezone name
            'queryString' => $event->getRequest()->getQueryString(),
            'isPlayerCardActive' => false, //TODO: Implement PlayerCard
            'captchakey' => $universe->getRecaptchapubkey(),
            'scripts' => [],
            'execscript' => '',

            //TODO(fionera): Support these settings
            'mailEnable' => false,
            'referralEnable' => false,
            'analyticsEnable' => false,
            'analyticsUID' => false,

            // We set the default to normal and override it in the controller if necessary
            'bodyclass' => 'normal',

            // Version related variables
            'VERSION' => "0", //TODO(fionera): Add version awareness
            'REV' => "0",
            'dpath'             => '/styles/theme/nova/', //TODO: Support themes

            // Locale related variables
            'LNG' => $this->translator->getCatalogue()->all('messages'),
            'lang' => $this->translator->getLocale(),
            'languages' => array_combine(
                $this->params->get('kernel.enabled_locales'),
                $this->params->get('kernel.enabled_locales')
            ),
        ];

        foreach ($vars as $k => $v) {
            $this->engine->assignGlobal($k, $v);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}

