<?php

namespace App\EventSubscriber;

use App\Entity\UniverseConfig;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UniverseResolveSubscriber implements EventSubscriberInterface
{

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $sessionUniverseID = $event->getRequest()->getSession()
            ->get(UniverseConfig::REQUEST_ATTRIBUTE, UniverseConfig::DEFAULT_UNIVERSE);

        $universeID = $event->getRequest()
            ->get(UniverseConfig::REQUEST_ATTRIBUTE, $sessionUniverseID);

        $universe = $this->entityManager->find(UniverseConfig::class, $universeID);
        if ($universe === null) {
            $universe = $this->entityManager->find(UniverseConfig::class, UniverseConfig::DEFAULT_UNIVERSE);
        }

        if ($sessionUniverseID !== $universe->getID()) {
            $event->getRequest()->getSession()->set(UniverseConfig::REQUEST_ATTRIBUTE, $universe->getID());
        }

        $event->getRequest()->attributes->set(UniverseConfig::REQUEST_ATTRIBUTE, $universe);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
