<?php

// VARS DB -> SCRIPT WRAPPER

$cache  = Cache::get();
$cache->add('vars', 'VarsBuildCache');
extract($cache->getData('vars'));

$resource[901] = 'metal';
$resource[902] = 'crystal';
$resource[903] = 'deuterium';
$resource[911] = 'energy';

$reslist['ressources']  = [901, 902, 903, 911];
$reslist['resstype'][1] = [901, 902, 903];
$reslist['resstype'][2] = [911];
$reslist['resstype'][3] = [];
