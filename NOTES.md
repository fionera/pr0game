Better battlehall:
```
create index uni1_topkb_time_universe_index
    on uni1_topkb (time, universe);

create index uni1_topkb_units_index
    on uni1_topkb (units desc);
```